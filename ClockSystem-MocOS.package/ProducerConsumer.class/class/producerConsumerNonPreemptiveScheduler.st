moc compositions
producerConsumerNonPreemptiveScheduler
|pc cc t1c t2c sec sfc sc clocks sys |
"producer code clocks"
pc := #(execute1 downE unblock1 upF critical1).
"consumer code clocks"
cc := #(execute2 downF unblock2 upE critical2).

"task1 clocks"
t1c := #(ready1 execute1 block1 unblock1 stop1).
"task2 clocks"
t2c := #(ready2 execute2 block2 unblock2 stop2).

"semaphore empty clocks"
sec := #(downE upE block1 unblock1).
"semaphore full clocks"
sfc := #(downF upF block2 unblock2).

"scheduler clocks"
sc := #(ready1 ready2 execute1 execute2 block1 block2 stop1 stop2).

clocks := (pc, cc, t1c, t2c, sec, sfc, sc) asSet.
sys := ClockSystem named: 'ProducerConsumer'.
sys addClocks: clocks.
sys library: ProducerConsumer relation: #code clocks: pc.
sys library: ProducerConsumer relation: #code clocks: cc.
sys library: ProducerConsumer relation: #task clocks: t1c.
sys library: ProducerConsumer relation: #task clocks: t2c.
sys library: ProducerConsumer relation: #semaphore clocks: sec variables: {10. OrderedCollection new}.
sys library: ProducerConsumer relation: #semaphore clocks: sfc variables: {0. OrderedCollection new}.
sys library: ProducerConsumer relation: #scheduler clocks: sc "constants: {false}" variables: {OrderedCollection new. -1}.
^sys.