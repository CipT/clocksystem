executionAPI
explore: sys
	| result |
	result := ClockCartesianProductBDDExploration exploreModel: sys.
	FileStream
		newFileNamed: (UITheme builder fileSave: 'res' extensions: {'gml'})
		do: [ :stream | TraConfigurationGraph2GML gml: result in: stream ].
	^ result