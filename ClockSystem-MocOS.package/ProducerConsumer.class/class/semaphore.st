mocs
semaphore
	"in this version the emptyCount can only block the producer. and the fullCount the consumer"
	"in this case we do not need the queue"
	"BUT: generally we need to know the ID of the guy doing the block to enqueue it so that unblock the first one"
	^[ :s :count :queue :down :up :block :unblock |
		s caseOf: { 
			[ 0 ] -> [ |transitions|
				transitions := OrderedCollection new.
				count > 0 ifTrue: [ 
						transitions add: (0->0 when: { down } do: [:c| c at: 2 put: (c at: 2) - 1 ]).
						transitions add: (0->0 when: { up } do: [:c| c at: 2 put: (c at: 2) + 1 ])]
					ifFalse: [ "down clockName = #downE ifTrue: [ self halt]."
						transitions add: (0->0 when: {down. block} do: [:c| (c at: 3) add: 1]).
						queue ifEmpty: [ transitions add: (0->0 when: { up } do: [:c| c at: 2 put: (c at: 2) + 1 ]) ]
							ifNotEmpty: [ transitions add: (0->0 when: {up. unblock} do: [:c| c at: 2 put: (c at: 2) + 1. (c at: 3) removeFirst])]].
					transitions.
				].
			
		}	
	
	]