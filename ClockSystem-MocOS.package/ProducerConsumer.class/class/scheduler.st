mocs
scheduler
	^[ :s :queue :eID :queue1 :queue2 :executing1 :executing2 :block1 :block2 :stop1 :stop2 |
	
		s caseOf: { 
			[ 0 ] -> [ |transitions|
				transitions := OrderedCollection new.
				transitions add: (0->0 when: {queue1} do: [:c | (c at: 2) add: 1]).
				transitions add: (0->0 when: {queue2} do: [:c | (c at: 2) add: 2]).
				queue ifNotEmpty: [ 
					queue first caseOf: { 
						[ 1 ] -> [ transitions add: (0->1 when: {executing1} do: [:c | (c at: 2) removeFirst. c at: 3 put: 1]) ].
						[ 2 ] -> [ transitions add: (0->1 when: {executing2} do: [:c | (c at: 2) removeFirst. c at: 3 put: 2]) ]
					}
				 ].
				transitions.
			].
			[ 1 ] -> [ |transitions|
				transitions := OrderedCollection new.
				eID = 2 ifTrue: [  
					transitions add: (1->1 when: {queue1} do: [:c | (c at: 2) add: 1]).
					transitions add: (1->1 when: {executing2}).
					transitions add: (1->0 when: {executing2. block2} do: [:c | c at: 3 put: -2 ])]
				ifFalse: [ 
					transitions add: (1->1 when: {queue2} do: [:c | (c at: 2) add: 2]).
					transitions add: (1->1 when: {executing1}).
					transitions add: (1->0 when: {executing1. block1} do: [:c | c at: 3 put: -2])].
				transitions. 
				].
		 }
	
	 ]