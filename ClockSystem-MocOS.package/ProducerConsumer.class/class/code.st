mocs
code

	^[ :s :run :down :unblock :up :cs |
		s caseOf: { 
			[ 0 ] -> [ { 0->1 when: { run } } ].
			[ 1 ] -> [ { 1->2 when: { run. down} } ].
			[ 2 ] -> [ { 2->3 when: { run. cs }. 2->1 when: { unblock } } ].
			[ 3 ] -> [ { 3->1 when: { run. up } } ].
		}
	]