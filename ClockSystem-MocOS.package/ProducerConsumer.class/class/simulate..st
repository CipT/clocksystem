executionAPI
simulate: sys
	| simtrace |
	simtrace := ClockBDDSimulation simulate: sys heuristic: #randomSAT:.
	[ | trace data model |
	trace := (ClockTraceExtractor from: simtrace policy: ClockRandomPolicy new) extract.
	data := (ClockTraceInterpreter run: trace for: 400) result.
	model := TicksModel new
		clocks: sys clocks;
		ticks: (data collect: [ :each | each vector asSet ]).
	MultiWaveView openOn: model waves ] value.
	^ simtrace