mocs
task
	|start waiting executing pending|
	start := 0. waiting := 1. executing := 2. pending := 3.
	^[ :s :ready :execute :block :unblock :stop |
		s caseOf: { 
			[ start ] -> [ { start->waiting when: { ready } } ].
			[ waiting ] -> [ { waiting->executing when: { execute } } ].
			[ executing ] -> [ { executing->executing when: { execute }. executing->pending when: { execute. block }. executing->start when: { stop } } ].
			[ pending ] -> [ { pending->start when: { unblock } } ].
		}
	]