mocs
scheduler2
	^[:s :queue :eID :isPreemptive :schedule1 :schedule2 :execute1 :execute2 :block1 :block2 :stop1 :stop2|
	s caseOf: {
		[0] -> [|transitions|
			transitions := OrderedCollection new.
			transitions add: (0->0 when: {schedule1} do:[:c | (c at: 2) add: 1]).
			transitions add: (0->0 when: {schedule2} do:[:c | (c at: 2) add: 2]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 1 ]) ifTrue:[
					0->1 when: {execute1} do: [:c | (c at: 2) removeFirst. c at: 3 put: 1]]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 2 ]) ifTrue:[
					0->1 when: {execute2} do: [:c | (c at: 2) removeFirst. c at: 3 put: 2]]).
			transitions reject: #isNil].
		[1] -> [|transitions|
			transitions := OrderedCollection new.
			eID=1 ifTrue: [
				transitions add: (1->1 when: {execute1}).
				transitions add: (1->0 when: {execute1. block1} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty])ifTrue: [1->0 when: {stop1} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule2} do:[:c | (c at: 2) add: 2]).
			].
			eID=2 ifTrue: [
				transitions add: (1->1 when: {execute2}).
				transitions add: (1->0 when: {execute2. block2} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty])ifTrue: [1->0 when: {stop2} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule1} do:[:c | (c at: 2) add: 1]).
			].
			transitions reject: #isNil]}]