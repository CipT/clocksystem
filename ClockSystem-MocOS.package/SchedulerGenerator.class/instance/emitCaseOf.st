emit code
emitCaseOf
	^'<t>s caseOf: {<n><1s><2s>}' expandMacrosWith: (self emit0Transitions) with: (self emit1Transitions).