emit signature
emitClocks: aName
	|clocks|
	clocks := '' writeStream.
	1 to: nbTasks do: [ :i | clocks nextPutAll: ' :', aName, i printString ].
	^clocks contents