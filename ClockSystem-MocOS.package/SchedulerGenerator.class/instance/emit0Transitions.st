emit code
emit0Transitions
	|code|
	code := '' writeStream.
	code 
		nextPutAll: '<t><t>[0] -> [|transitions|<n>' expandMacros;
		nextPutAll: '<t><t><t>transitions := OrderedCollection new.<n>' expandMacros;
		nextPutAll: ('<1s>' expandMacrosWith: self emitStartingTransitions);
		nextPutAll: '<t><t><t>transitions reject: #isNil].<n>' expandMacros.
	^code contents