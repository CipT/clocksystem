emit transitions
emitStartExecuting: taskID
	^'<t><t><t><t>((queue isNotEmpty and: [ queue first = <1p> ]) ifTrue:[<n><t><t><t><t><t>0->1 when: {execute<1p>} do: [:c | (c at: 2) removeFirst. c at: 3 put: <1p>]])' expandMacrosWith: taskID