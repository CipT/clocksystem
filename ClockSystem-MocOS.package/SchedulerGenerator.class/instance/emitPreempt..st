emit transitions
emitPreempt: taskID
	^'((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop<1p>} do: [:c | c at: 3 put: 0]])' expandMacrosWith: taskID