generation
generateScheduler: n
	|automataBlock code|
	nbTasks := n.
	automataBlock := self emitScheduler.
	code := 'scheduler<1p>Tasks<n><t>^<2s>' expandMacrosWith: n with: automataBlock.
	ProducerConsumer compile: code.