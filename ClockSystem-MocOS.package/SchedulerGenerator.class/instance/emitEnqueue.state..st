emit transitions
emitEnqueue: processID state: stateID

	^'(<1p>-><1p> when: {schedule<2p>} do:[:c | (c at: 2) add: <2p>])' expandMacrosWith: stateID with: processID 