emit transitions
emitExecuting: taskID
	^'(1->1 when: {execute<1p>})' expandMacrosWith: taskID