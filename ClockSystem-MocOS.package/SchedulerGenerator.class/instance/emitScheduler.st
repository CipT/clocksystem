emit code
emitScheduler
	^'[<1s>|<n><2s>]' expandMacrosWith: self emitArguments with: self emitCaseOf.