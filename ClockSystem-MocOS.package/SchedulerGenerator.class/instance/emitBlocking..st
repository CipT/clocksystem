emit transitions
emitBlocking: taskID
	^'(1->0 when: {execute<1p>. block<1p>} do: [:c | c at: 3 put: 0])' expandMacrosWith: taskID