emit code
emit1Transitions
	|code|
	code := '' writeStream.
	code 
		nextPutAll: '<t><t>[1] -> [|transitions|<n>' expandMacros;
		nextPutAll: '<t><t><t>transitions := OrderedCollection new.<n>' expandMacros.
	1 to: nbTasks do: [ :each |
		code nextPutAll: (self emitExecutingTransitions: each).
	].
	code
		nextPutAll: '<t><t><t>transitions reject: #isNil]' expandMacros.
	^code contents