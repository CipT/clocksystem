emit signature
emitArguments
	^':s :queue :eID :isPreemptive'
	, (self emitClocks: 'schedule')
	, (self emitClocks: 'execute')
	, (self emitClocks: 'block')
	, (self emitClocks: 'stop')
	