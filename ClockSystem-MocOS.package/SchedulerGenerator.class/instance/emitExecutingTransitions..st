emit code
emitExecutingTransitions: taskID
	|others code|
	others := (1 to: nbTasks) asSet \ {taskID}.
	code := '' writeStream.
	code 
		nextPutAll: ('<t><t><t>eID=<1p> ifTrue: [<n>' expandMacrosWith: taskID);
		nextPutAll: ('<t><t><t><t>transitions add: <1s>.<n>' expandMacrosWith: (self emitExecuting: taskID));
		nextPutAll: ('<t><t><t><t>transitions add: <1s>.<n>' expandMacrosWith: (self emitBlocking: taskID));
		nextPutAll: ('<t><t><t><t>transitions add: <1s>.<n>' expandMacrosWith: (self emitPreempt: taskID)).
	others do: [ :each | 
		code nextPutAll: ('<t><t><t><t>transitions add: <1s>.<n>' expandMacrosWith: (self emitEnqueue: each state: 1))].
	code nextPutAll: '<t><t><t>].<n>' expandMacros.
	^code contents