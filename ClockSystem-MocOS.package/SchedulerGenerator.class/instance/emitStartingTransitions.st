emit code
emitStartingTransitions
	| code|
	code := '' writeStream.
	1 to: nbTasks  do: [ :each | 
		code nextPutAll: ('<t><t><t>transitions add: <1s>.<n>' expandMacrosWith: (self emitEnqueue: each state: 0))].
	1 to: nbTasks do: [ :each |
		code nextPutAll: ('<t><t><t>transitions add:<n><1s>.<n>' expandMacrosWith: (self emitStartExecuting: each)).
		 ].
	^code contents