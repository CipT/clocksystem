as yet unclassified
relationDefinitionBlock
	^[state caseOf: {
		[0] -> [|transitions|
			transitions := OrderedCollection new.
			transitions add: (0->0 when: {schedule1} do:[:c | (c at: 2) add: 1]).
			transitions add: (0->0 when: {schedule2} do:[:c | (c at: 2) add: 2]).
			transitions add: (0->0 when: {schedule3} do:[:c | (c at: 2) add: 3]).
			transitions add: (0->0 when: {schedule4} do:[:c | (c at: 2) add: 4]).
			transitions add: (0->0 when: {schedule5} do:[:c | (c at: 2) add: 5]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 1 ]) ifTrue:[
					0->1 when: {execute1} do: [:c | (c at: 2) removeFirst. c at: 3 put: 1]]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 2 ]) ifTrue:[
					0->1 when: {execute2} do: [:c | (c at: 2) removeFirst. c at: 3 put: 2]]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 3 ]) ifTrue:[
					0->1 when: {execute3} do: [:c | (c at: 2) removeFirst. c at: 3 put: 3]]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 4 ]) ifTrue:[
					0->1 when: {execute4} do: [:c | (c at: 2) removeFirst. c at: 3 put: 4]]).
			transitions add:
				((queue isNotEmpty and: [ queue first = 5 ]) ifTrue:[
					0->1 when: {execute5} do: [:c | (c at: 2) removeFirst. c at: 3 put: 5]]).
			transitions reject: #isNil].
		[1] -> [|transitions|
			transitions := OrderedCollection new.
			eID=1 ifTrue: [
				transitions add: (1->1 when: {execute1}).
				transitions add: (1->0 when: {execute1. block1} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop1} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule2} do:[:c | (c at: 2) add: 2]).
				transitions add: (1->1 when: {schedule3} do:[:c | (c at: 2) add: 3]).
				transitions add: (1->1 when: {schedule4} do:[:c | (c at: 2) add: 4]).
				transitions add: (1->1 when: {schedule5} do:[:c | (c at: 2) add: 5]).
			].
			eID=2 ifTrue: [
				transitions add: (1->1 when: {execute2}).
				transitions add: (1->0 when: {execute2. block2} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop2} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule1} do:[:c | (c at: 2) add: 1]).
				transitions add: (1->1 when: {schedule3} do:[:c | (c at: 2) add: 3]).
				transitions add: (1->1 when: {schedule4} do:[:c | (c at: 2) add: 4]).
				transitions add: (1->1 when: {schedule5} do:[:c | (c at: 2) add: 5]).
			].
			eID=3 ifTrue: [
				transitions add: (1->1 when: {execute3}).
				transitions add: (1->0 when: {execute3. block3} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop3} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule1} do:[:c | (c at: 2) add: 1]).
				transitions add: (1->1 when: {schedule2} do:[:c | (c at: 2) add: 2]).
				transitions add: (1->1 when: {schedule4} do:[:c | (c at: 2) add: 4]).
				transitions add: (1->1 when: {schedule5} do:[:c | (c at: 2) add: 5]).
			].
			eID=4 ifTrue: [
				transitions add: (1->1 when: {execute4}).
				transitions add: (1->0 when: {execute4. block4} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop4} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule1} do:[:c | (c at: 2) add: 1]).
				transitions add: (1->1 when: {schedule2} do:[:c | (c at: 2) add: 2]).
				transitions add: (1->1 when: {schedule3} do:[:c | (c at: 2) add: 3]).
				transitions add: (1->1 when: {schedule5} do:[:c | (c at: 2) add: 5]).
			].
			eID=5 ifTrue: [
				transitions add: (1->1 when: {execute5}).
				transitions add: (1->0 when: {execute5. block5} do: [:c | c at: 3 put: 0]).
				transitions add: ((isPreemptive and: [queue isNotEmpty]) ifTrue: [1->0 when: {stop5} do: [:c | c at: 3 put: 0]]).
				transitions add: (1->1 when: {schedule1} do:[:c | (c at: 2) add: 1]).
				transitions add: (1->1 when: {schedule2} do:[:c | (c at: 2) add: 2]).
				transitions add: (1->1 when: {schedule3} do:[:c | (c at: 2) add: 3]).
				transitions add: (1->1 when: {schedule4} do:[:c | (c at: 2) add: 4]).
			].
			transitions reject: #isNil]}]