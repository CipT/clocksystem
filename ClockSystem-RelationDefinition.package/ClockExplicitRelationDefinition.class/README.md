This class represent an explicit relation definition in clock system. The objects of this class encodes the clock relation as an explicit list of transitions 

A transition is something like: 
[ guard ] { clocks } [ action ]