as yet unclassified
relationsInAGivenState
	|s|
	isPreemptive := false.
	state := 0.
	queue := OrderedCollection new.
	eID := 0.
	s := ClockSystem named: #s.
	schedule1 := s clock: #s1.
	schedule2 := s clock: #s2.
	execute1 := s clock: #e1.
	execute2 := s clock: #e2.
	block1 := s clock: #b1.
	block2 := s clock: #b2.
	stop1 := s clock: #st1.
	stop2 := s clock: #st2.
	^self relationDefinitionBlock value.