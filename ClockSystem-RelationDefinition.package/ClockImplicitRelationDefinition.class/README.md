This class represent an implicit relation definition in clock system. The objects of this class encodes the clock relation as an ST80 block which returns a list of transition when it is executed with a given context.

A transition is something like: 
[ guard ] { clocks } [ actions ].
In this case the guard should be evaluated before returning the list of transitions.
Moreover since the relation is represented as a ST80 block most likely there is no need for the guard expression.
{ clocks } [ actions ] should be enough