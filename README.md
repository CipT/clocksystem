# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


git clone 

```
#!smalltalk
Gofer new 
	url: 'filetree://<local-path>'; 
	package: 'ConfigurationOfClockSystem'; 
	load. 

(Smalltalk at: #ConfigurationOfClockSystem) 
	gitRepoPath: '<local-path>';
	loadDevelopment .
```
	

Simulation: 


```
#!smalltalk

system := ExampleSDF example4mocFFT4Date14_16_6_platform24 system.
simtrace := ClockBDDSimulation 
	simulate: system
	heuristic: #randomSAT:.
	
[|   trace data model |
	trace := (ClockTraceExtractor from: simtrace policy: ClockRandomPolicy new) extract .
	data := (ClockTraceInterpreter run: trace for: 200) result.
	
model := TicksModel new
	clocks: (#('Signal Provider' Display1 AVG NFFT Threshold Display2) collect: [:each | system perform: each]);
	ticks: (data collect: [:each | each vector asSet]).
	
	MultiWaveView openOn: model waves.
] value.

```




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ciprian.Teodorov
* Other community or team contact

### TO DO ###
* [done] remove dependency with Glamour by creation Spec-based editor/model browser
* [done] implement allCoincide and allExclusive as one automata because it will be faster
* [done] add clear messages if java path and ClockRDL jar are not found
* [done] open-file button does not seem to work -- fixed
* [] improve state-space storage ... maybe by merging with Plug
* [] implement shared variables & the exploration of synchronizations
```
#!smalltalk
ClockParallelComposition>>execute: transitions configuration: configuration notEmptyIdx: notEmptyIdx clockStates: clockStates
```