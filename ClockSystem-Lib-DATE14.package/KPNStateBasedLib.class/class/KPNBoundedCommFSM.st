as yet unclassified
KPNBoundedCommFSM
	|one|
	one := 1.
	^ [:s :dCurrent :vbool :bzero :KBtotalRead :KBtotalWrite :maxSize :KBMemo_reading :KBMemo_writing|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(vbool == one) ifTrue: [
								transitions add: (0 -> 0  when:{KBMemo_reading. KBMemo_writing} 
								) ].							(dCurrent - KBtotalRead >= bzero) ifTrue: [
								transitions add: (0 -> 0  when:{KBMemo_reading}  do: [:configuration |
									|dCurrent_ | 
									dCurrent_ := configuration at: 2.
									configuration at: 2 put: (dCurrent_ - KBtotalRead).

								]
								) ].							(dCurrent + KBtotalWrite <= maxSize) ifTrue: [
								transitions add: (0 -> 0  when:{KBMemo_writing}  do: [:configuration |
									|dCurrent_ | 
									dCurrent_ := configuration at: 2.
									configuration at: 2 put: (dCurrent_ + KBtotalWrite).

								]
								) ].							
								transitions asArray.
						].

				
				} "end caseof"

		]. "end #KPNBoundedCommFSM"