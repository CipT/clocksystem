as yet unclassified
KPNCommFSM 
	|one|
	one := 1.
	^ [:s :currentData :kzero :xbool  :KMemo_reading :KMemo_writing|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(currentData > kzero) ifTrue: [
								transitions add: (0 -> 0  when:{KMemo_reading}  do: [:configuration |
									|currentData_ | 
									currentData_ := configuration at: 2.
									configuration at: 2 put: (currentData_ - one).

								]
								) ].							(xbool == one) ifTrue: [
								transitions add: (0 -> 0  when:{KMemo_writing}  do: [:configuration |
									|currentData_ | 
									currentData_ := configuration at: 2.
									configuration at: 2 put: (currentData_ + one).

								]
								) ].							
								transitions asArray.
						].

				
				} "end caseof"

		]. "end #KPNCommFSM"