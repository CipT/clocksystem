as yet unclassified
actor: name cycles: nbCycles

	|start execute stop|
	start := system clock: ('start', name) asSymbol.
	execute := system clock: ('execute', name) asSymbol.
	stop := system clock: ('stop', name) asSymbol.
	
	system library: #SDFStateBasedLib relation: #actorRelationDef clocks: { start. execute. stop } constants: { nbCycles } variables: { 0 }.