as yet unclassified
edgeFrom: source to: target outRate: out initial: initialTokens inRate: in capacity: capacity
	| read write |
	read := system clock: ('rchan' , idx printString) asSymbol.
	write := system clock: ('wchan' , idx printString) asSymbol.
	idx := idx + 1.
	system
		library: #SDFStateBasedLib
		relation: #outputRelationDef
		clocks: {write. source}
		constants: {out}
		variables: {out}.
	system
		library: #SDFStateBasedLib
		relation: #edgeRelationDef
		clocks: {read. write}
		constants: {capacity}
		variables: {initialTokens}.
	system
		library: #SDFStateBasedLib
		relation: #inputRelationDef
		clocks: {read. target}
		constants: {in}
		variables: {0}