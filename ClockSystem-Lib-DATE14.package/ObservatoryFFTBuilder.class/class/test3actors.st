as yet unclassified
test3actors
	|sys|
	sys := ClockSystem named: 'observatoryFFT16'.
	^(self on: sys)
		actor: #SP cycles: 1;
		actor: #Display1 cycles: 1;
		actor: #FFT cycles: 1;
		edgeFrom: sys stopSP to: sys startDisplay1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: sys stopSP to: sys startFFT outRate: 1 initial: 0 inRate: 8 capacity: 8.