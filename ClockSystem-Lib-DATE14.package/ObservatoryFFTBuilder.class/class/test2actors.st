as yet unclassified
test2actors
	|sys|
	sys := ClockSystem named: 'observatoryFFT16'.
	^(self on: sys)
		actor: #SP cycles: 1;
		actor: #Display1 cycles: 1;
		edgeFrom: sys stopSP to: sys startDisplay1 outRate: 1 initial: 0 inRate: 1 capacity: 1.
		