as yet unclassified
observatoryFFT16
	|sys|
	sys := ClockSystem named: 'observatoryFFT16'.
	^(self on: sys)
		actor: #SP cycles: 1;
		actor: #Display1 cycles: 1;
		actor: #FFT cycles: 1;
		actor: #AVG cycles: 1;
		actor: #Threshold cycles: 1;
		actor: #Display2 cycles: 1;
		edgeFrom: sys stopSP to: sys startDisplay1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: sys stopSP to: sys startFFT outRate: 1 initial: 0 inRate: 16 capacity: 16;
		edgeFrom: sys stopFFT to: sys startThreshold outRate: 16 initial: 0 inRate: 4 capacity: 16;
		edgeFrom: sys stopFFT to: sys startAVG outRate: 16 initial: 16 inRate: 8 capacity: 16;
		edgeFrom: sys stopAVG to: sys startFFT outRate: 8 initial: 0 inRate: 16 capacity: 16;
		edgeFrom: sys stopThreshold to: sys startDisplay2 outRate: 1 initial: 0 inRate: 1 capacity: 1.
		