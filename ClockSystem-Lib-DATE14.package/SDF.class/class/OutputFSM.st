as yet unclassified
OutputFSM
	|zero one|
	zero := 0. one := 1.
	 ^[:s :orcounter :Output_rate_fsm :Output_actor_fsm :Output_write_fsm|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(orcounter == zero) ifTrue: [
								transitions add: (0 -> 1  when:{Output_actor_fsm} 
								) ].							(orcounter == zero) ifTrue: [
								transitions add: (0 -> 1  when:{Output_actor_fsm. Output_write_fsm}  do: [:configuration |
									|orcounter_ | 
									orcounter_ := configuration at: 2.
									configuration at: 2 put: (orcounter_ + one).

								]
								) ].							
								transitions asArray.
						].

						[1] -> [|transitions|
						transitions := OrderedCollection new.
						(orcounter < Output_rate_fsm) ifTrue:[
								transitions add: (1 -> 1  when:{Output_write_fsm}  do: [:configuration |
									|orcounter_ |
									orcounter_ := configuration at: 2.
									configuration at: 2 put: (orcounter_ + one).

								]
								) ].

						(orcounter == Output_rate_fsm) ifTrue:[
								transitions add: (1 -> 1  when:{Output_write_fsm. Output_actor_fsm}  do: [:configuration |
									|orcounter_ |
									orcounter_ := configuration at: 2.
									configuration at: 2 put: (orcounter_ - Output_rate_fsm + one).

								]
								) ].

						(orcounter == Output_rate_fsm) ifTrue:[
								transitions add: (1 -> 1  when:{Output_actor_fsm}  do: [:configuration |
									|orcounter_ |
									orcounter_ := configuration at: 2.
									configuration at: 2 put: (orcounter_ - Output_rate_fsm).

								]
								) ].


								transitions asArray.
						].

				
				} "end caseof"

		]. "end #OutputFSM"