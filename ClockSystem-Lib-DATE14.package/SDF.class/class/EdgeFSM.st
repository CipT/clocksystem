as yet unclassified
EdgeFSM
	|zero one|
	zero := 0. one := 1.
 	^[:s :val :valSize :Edge_delay :Edge_size :Edge_write :Edge_read|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(val == zero) ifTrue: [
								transitions add: (0 -> 1  do: [:configuration |
									|valSize_ val_ | 
									valSize_ := configuration at: 3.
									val_ := configuration at: 2.
									configuration at: 3 put: (Edge_size).
									configuration at: 2 put: (Edge_delay).

								]
								) ].							
								transitions asArray.
						].

						[1] -> [|transitions|
						transitions := OrderedCollection new.
						(val > zero) ifTrue:[
								transitions add: (1 -> 1  when:{Edge_read}  do: [:configuration |
									|val_ |
									val_ := configuration at: 2.
									configuration at: 2 put: (val_ - one).

								]
								) ].

						(val < valSize) ifTrue:[
								transitions add: (1 -> 1  when:{Edge_write}  do: [:configuration |
									|val_ |
									val_ := configuration at: 2.
									configuration at: 2 put: (val_ + one).

								]
								) ].

						(val > zero) ifTrue:[
								transitions add: (1 -> 1  when:{Edge_write. Edge_read} 
								) ].


								transitions asArray.
						].

				
				} "end caseof"

		]. "end #EdgeFSM"