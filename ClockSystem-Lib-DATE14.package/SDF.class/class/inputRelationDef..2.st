handwritten-lib
inputRelationDef
	^ [ :s :size :inRate :read :actor | |transitions|
		transitions := OrderedCollection new.
		size < inRate ifTrue: [ transitions add: ( 0->0 when: { read } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: size_ + 1 ] ) ].
		size = inRate ifTrue: [ transitions add: ( 0->0 when: { actor } do: [ :configuration |
				configuration at: 2 put: 0 ] ) ].
		"size = inRate ifTrue: [ transitions add: ( 0->0 when: { read. actor } do: [ :configuration |
				configuration at: 2 put: 1 ] ) ]."
		transitions]