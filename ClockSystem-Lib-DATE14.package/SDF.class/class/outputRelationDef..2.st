handwritten-lib
outputRelationDef
	^ [ :s :size :outRate :write :actor | |transitions|
		transitions := OrderedCollection new.
		size < outRate ifTrue: [ transitions add: ( 0->0 when: { write } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: size_ + 1 ] ) ].
		size = outRate ifTrue: [ transitions add: ( 0->0 when: { actor } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: 0 ] ) ].
		"size = outRate ifTrue: [ transitions add: ( 0->0 when: { write. actor } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: 1 ] ) ]."
		transitions]