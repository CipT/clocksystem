as yet unclassified
OutputRelationDef
	^[:s :orcounter :ozero :Output_rate_param :Output_actor_param :Output_write_param|
	
				s caseOf: {
							[0] -> [|transitions|
							transitions := OrderedCollection new.
							
									transitions add: (0 -> 1  do: [:configuration |
										|orcounter_ | 
										orcounter_ := configuration at: 2.
										configuration at: 2 put: (0).

									]
									)  .							
									transitions asArray.
							].

							[1] -> [|transitions|
							transitions := OrderedCollection new.
							(orcounter == ozero) ifTrue:[
									transitions add: (1 -> 2  when:{Output_actor_param} 
									) ].

							(orcounter == ozero) ifTrue:[
									transitions add: (1 -> 2  when:{Output_actor_param. Output_write_param}  do: [:configuration |
										|orcounter_ |
										orcounter_ := configuration at: 2.
										configuration at: 2 put: (orcounter_ + 1).
	
									]
									) ].


									transitions asArray.
							].

							[2] -> [|transitions|
							transitions := OrderedCollection new.
							(orcounter < Output_rate_param) ifTrue:[
									transitions add: (2 -> 2  when:{Output_write_param}  do: [:configuration |
										|orcounter_ |
										orcounter_ := configuration at: 2.
										configuration at: 2 put: (orcounter_ + 1).
	
									]
									) ].

							(orcounter == Output_rate_param) ifTrue:[
									transitions add: (2 -> 2  when:{Output_write_param. Output_actor_param}  do: [:configuration |
										|orcounter_ |
										orcounter_ := configuration at: 2.
										configuration at: 2 put: (orcounter_ - Output_rate_param + 1).
	
									]
									) ].

							(orcounter == Output_rate_param) ifTrue:[
									transitions add: (2 -> 2  when:{Output_actor_param}  do: [:configuration |
										|orcounter_ |
										orcounter_ := configuration at: 2.
										configuration at: 2 put: (ozero).
	
									]
									) ].


									transitions asArray.
							].

					
					} "end caseof"
 
			]. "end #OutputRelationDef"