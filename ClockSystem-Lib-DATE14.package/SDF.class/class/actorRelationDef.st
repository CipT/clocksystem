handwritten-lib
actorRelationDef
	^[:s :cycle :nbCycles :start :execute :stop | |transitions|
		transitions := OrderedCollection new.
		(s = 0 and: [ cycle < nbCycles ]) ifTrue: [ 
			transitions add: (0->1 when: { start }) ].
		(s = 1 and: [ cycle < nbCycles ]) ifTrue: [ 
			transitions add: ( 1->1 when: { execute } do: [ :configuration | 
				|cycle_|
				cycle_ := configuration at: 2.
				configuration at: 2 put: (cycle_ + 1)] )
			].
		(s = 1 and: [ cycle = nbCycles]) ifTrue: [ 
			transitions add: ( 1->0 when: { stop } do: [ :configuration |
				"reset cycle"
				configuration at: 2 put: 0] )
			].
		transitions]