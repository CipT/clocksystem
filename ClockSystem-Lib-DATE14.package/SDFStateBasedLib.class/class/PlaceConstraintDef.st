as yet unclassified
PlaceConstraintDef 
|one|
one := 1.
^[:s :currentSize :maxSize :ezero :Place_delay :Place_size :Place_popRate :Place_pushRate :Place_write :Place_read|
	
				s caseOf: {
							0 ->[ |transitions|
							transitions := OrderedCollection new.
							(currentSize == ezero) ifTrue: [  
									transitions add: (0 -> 1  do: [:configuration |
										|maxSize_ currentSize_ | 
										maxSize_ := configuration at: 3.
										currentSize_ := configuration at: 2.
										configuration at: 3 put: (Place_size).
										configuration at: 2 put: (Place_delay).

									
									]) ].							
									transitions asArray.
							].

							1 ->[ |transitions|
							transitions := OrderedCollection new.
							(currentSize > (Place_popRate - one)) ifTrue:[
									transitions add: (1 -> 1  when:{}  do: [:configuration |
										|currentSize_ |
										currentSize_ := configuration at: 2.
										configuration at: 2 put: (currentSize_ - Place_popRate).
	
									
									])] .

							((currentSize + Place_pushRate) < (maxSize + one)) ifTrue:[
									transitions add: (1 -> 1  when:{}  do: [:configuration |
										|currentSize_ |
										currentSize_ := configuration at: 2.
										configuration at: 2 put: (currentSize_ + Place_pushRate).
	
									
									])] .


									transitions asArray.
							].

					
					} "end caseof"
 
			]. "end #PlaceConstraintDef"

