as yet unclassified
AgentExecutionFSM
	| zero one |
	zero := 0. one := 1.
	^ [:s :tcycle :cycleVal :AgentCycle :AgentExec_start :AgentExec_stop :AgentisExec|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(cycleVal == zero) ifTrue: [
								transitions add: (0 -> 1  do: [:configuration |
									|cycleVal_ | 
									cycleVal_ := configuration at: 3.
									configuration at: 3 put: (AgentCycle).

								]
								) ].							
								transitions asArray.
						].

						[1] -> [|transitions|
						transitions := OrderedCollection new.
						(tcycle < cycleVal) ifTrue:[
								transitions add: (1 -> 2  when:{AgentExec_start} 
								) ].


								transitions asArray.
						].

						[2] -> [|transitions|
						transitions := OrderedCollection new.
						(tcycle < cycleVal) ifTrue:[
								transitions add: (2 -> 2  when:{AgentisExec}  do: [:configuration |
									|tcycle_ |
									tcycle_ := configuration at: 2.
									configuration at: 2 put: (tcycle_ + one).

								]
								) ].

						(tcycle == cycleVal) ifTrue:[
								transitions add: (2 -> 1  when:{AgentExec_stop}  do: [:configuration |
									|tcycle_ |
									tcycle_ := configuration at: 2.
									configuration at: 2 put: (tcycle_ - cycleVal).

								]
								) ].


								transitions asArray.
						].

				
				} "end caseof"

		]. "end #AgentExecutionFSM"