handwritten-lib
edgeRelationDef
	^ [ :s :size :capacity :read :write | |transitions|
		transitions := OrderedCollection new.
		size > 0 ifTrue: [ transitions add: ( 0->0 when: { read } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: size_ - 1 ] ) ].
		size < capacity ifTrue: [ transitions add: ( 0->0 when: { write } do: [ :configuration |
				|size_|
				size_ := configuration at: 2.
				configuration at: 2 put: size_ + 1 ] ) ].
		"(size > 0 and: [ size < capacity ]) ifTrue: [ transitions add: ( 0->0 when: { read. write } ) ]."
		transitions]