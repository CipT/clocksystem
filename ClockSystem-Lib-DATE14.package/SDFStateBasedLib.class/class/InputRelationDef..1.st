as yet unclassified
InputRelationDef
	^[:s :rcounter :izero :rateCons :Input_rate_param :Input_actor_param :Input_read_param|
	
				s caseOf: {
							[0] -> [|transitions|
							transitions := OrderedCollection new.
							(rateCons == izero) ifTrue: [
									transitions add: (0 -> 1  do: [:configuration |
										|rateCons_ | 
										rateCons_ := configuration at: 4.
										configuration at: 4 put: (Input_rate_param).

									]
									) ].							
									transitions asArray.
							].

							[1] -> [|transitions|
							transitions := OrderedCollection new.
							(rcounter < rateCons) ifTrue:[
									transitions add: (1 -> 1  when:{Input_read_param}  do: [:configuration |
										|rcounter_ |
										rcounter_ := configuration at: 2.
										configuration at: 2 put: (rcounter_ + 1).
	
									]
									) ].

							(rcounter == rateCons) ifTrue:[
									transitions add: (1 -> 1  when:{Input_actor_param}  do: [:configuration |
										|rcounter_ |
										rcounter_ := configuration at: 2.
										configuration at: 2 put: (izero).
	
									]
									) ].


									transitions asArray.
							].

					
					} "end caseof"
 
			]. "end #InputRelationDef"