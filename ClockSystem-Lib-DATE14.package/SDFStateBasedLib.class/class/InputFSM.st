as yet unclassified
InputFSM
	|zero one|
	zero := 0. one := 1.
	^[:s :rcounter :rateCons :rateConsMinusOne :Input_rate_fsm :Input_actor_fsm :Input_read_fsm|

			s caseOf: {
						[0] -> [|transitions|
						transitions := OrderedCollection new.
						(rateCons == zero) ifTrue: [
								transitions add: (0 -> 1  do: [:configuration |
									|rateCons_ rateConsMinusOne_ | 
									rateCons_ := configuration at: 3.
									rateConsMinusOne_ := configuration at: 4.
									configuration at: 3 put: (Input_rate_fsm).
									configuration at: 4 put: (Input_rate_fsm - one).

								]
								) ].							
								transitions asArray.
						].

						[1] -> [|transitions|
						transitions := OrderedCollection new.
						(rcounter < rateCons) ifTrue:[
								transitions add: (1 -> 1  when:{Input_read_fsm}  do: [:configuration |
									|rcounter_ |
									rcounter_ := configuration at: 2.
									configuration at: 2 put: (rcounter_ + one).

								]
								) ].

						(rcounter == rateConsMinusOne) ifTrue:[
								transitions add: (1 -> 1  when:{Input_actor_fsm. Input_read_fsm}  do: [:configuration |
									|rcounter_ |
									rcounter_ := configuration at: 2.
									configuration at: 2 put: (rcounter_ - rateConsMinusOne).

								]
								) ].

						(rcounter == rateCons) ifTrue:[
								transitions add: (1 -> 1  when:{Input_actor_fsm}  do: [:configuration |
									|rcounter_ |
									rcounter_ := configuration at: 2.
									configuration at: 2 put: (rcounter_ - rateCons).

								]
								) ].

						(rcounter == rateCons) ifTrue:[
								transitions add: (1 -> 1  when:{Input_actor_fsm. Input_read_fsm}  do: [:configuration |
									|rcounter_ |
									rcounter_ := configuration at: 2.
									configuration at: 2 put: (rateCons - rateConsMinusOne).

								]
								) ].


								transitions asArray.
						].

				
				} "end caseof"

		]. "end #InputFSM"