private
clockAssignmentsIn: aConfiguration transitions: enabledTransitions
	^ self satSolver
		enabledTransitions: enabledTransitions;
		allSolutions: aConfiguration