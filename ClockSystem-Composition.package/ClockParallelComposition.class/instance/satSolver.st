accessing
satSolver
	^ satSolver ifNil: [ satSolver := self defaultSolver ] ifNotNil: [ satSolver ]