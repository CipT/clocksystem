private
initialConfiguration
	^ (Array new: automata size streamContents: [ :stream | automata do: [ :each | stream nextPut: each startState ] ])