private
execute: transitions configuration: configuration notEmptyIdx: notEmptyIdx clockStates: clockStates
	| newConfig |
	newConfig := configuration deepCopy.
	transitions
		withIndexDo: [ :transition :nEIDX | 
			| aidx |
			aidx := notEmptyIdx at: nEIDX.	
			"advance to the new state with the transition"
			(automata at: aidx) do: transition withState: (newConfig at: aidx) ].
	(known includes: newConfig)
		ifFalse: [ 
			known add: newConfig.
			toSee addLast: newConfig ].
	"graph at: configuration add: (newConfig -> (self tickingClocks: clockStates))."
	graph addTransition: (configuration -> newConfig when: (self tickingClocks: clockStates))