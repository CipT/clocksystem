exploration api
next
	| configuration transitionsPerAutomaton assignments |
	configuration := toSee removeFirst.
	transitionsPerAutomaton := Array new: automata size withAll: Array empty. "self halt."
	assignments := self clockAssignmentsIn: configuration transitions: transitionsPerAutomaton.
	assignments := self filterClockAssignments: assignments.
	assignments
		do: [ :clockValuation | 
"			(clockValuation reject: [ :clockState | clockState = Tristate notick ])
				ifNotEmpty: [ "
					| activeAutomataAndFireable |
					"from all the enabled transitions, select the fireable ones according to the current clockStates"
					activeAutomataAndFireable := self fireable: transitionsPerAutomaton withClocks: clockValuation.
					activeAutomataAndFireable second
						ifNotEmpty: [ 
							"execute the fireable transitions"
							self perform: activeAutomataAndFireable configuration: configuration withClocks: clockValuation ] ] "]"