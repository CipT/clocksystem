private
fireable: transitionsPerAutomaton withClocks: clockStates
	| fireablePerAutomata notEmptyIdx |
	"from all the enabled transitions, select the fireable ones according to the current clockStates"
	fireablePerAutomata := OrderedCollection new: automata size.	
	notEmptyIdx := OrderedCollection new.
	transitionsPerAutomaton
		withIndexDo: [ :enabledTransitions :aidx |
			| fireable |
			fireable := enabledTransitions select: [ :transition | transition isFireableIn: (automata at: aidx) with:  clockStates ].
			fireable isEmpty
				ifFalse: [ 
					notEmptyIdx add: aidx.
					fireablePerAutomata add: fireable ] ].
	^{notEmptyIdx. fireablePerAutomata}