private
initializeExploration
	|initialConfiguration|
	graph := ClockConfigurationGraph system: model.
	clocks := model clocks asOrderedCollection.
	clocks doWithIndex: [ :each :idx | each index: idx ].
	automata := model unconstrainedClockAutomata, model relations.
	initialConfiguration := self initialConfiguration.
	known := Set with: initialConfiguration.
	toSee := OrderedCollection with: initialConfiguration.
	graph initial: initialConfiguration.