private
tickingClocks: clockStates
	| clks |
	clks := OrderedCollection new.
	clockStates
		withIndexDo: [ :state :idx | 
			state = Tristate tick
				ifTrue: [ clks add: (clocks at: idx) ] ].
	^ clks