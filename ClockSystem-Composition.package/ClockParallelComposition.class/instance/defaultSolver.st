accessing
defaultSolver
	| solverClass |
	solverClass := Smalltalk
		at: #ClockTristateSatisfiability
		ifAbsent: [ 
			self error: 'Tristate solver class not found'.
			^ nil ].
	^ solverClass new model: self