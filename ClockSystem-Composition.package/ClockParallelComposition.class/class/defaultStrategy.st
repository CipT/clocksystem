as yet unclassified
defaultStrategy
	^ thisContext receiver = ClockParallelComposition  
		ifTrue: [ self compositionClass ]
		ifFalse: [ thisContext receiver ]