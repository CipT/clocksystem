adding
at: aConfiguration add: aTransition
	|adjSet|
	adjSet := adjacencySet at: aConfiguration ifAbsentPut: [ Set new ].
	adjSet add: aTransition.