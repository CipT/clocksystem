exploration api
perform: activeAutomataAndFireable configuration: configuration withClocks: clockStates
	| fireablePerAutomata nEIDX |
	"from all the enabled transitions, select the fireable ones according to the current clockStates"
	nEIDX := activeAutomataAndFireable first.
	fireablePerAutomata := activeAutomataAndFireable second.	
	"Compute a cartesian product between the sets of fireable transitions per automata"
	cartesianProductInstance 
		cartesianProductIterative: fireablePerAutomata
		do: [ :toFire | 
			self
				execute: toFire
				configuration: configuration
				notEmptyIdx: nEIDX
				clockStates: clockStates ]