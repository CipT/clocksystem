extracting
extract
	| current hasNext |
	current := ccg initial.
	hasNext := true.
	[ (known includes: current) or: [ hasNext not ] ]
		whileFalse: [ 
			| next fanout |
			known add: current.
			fanout := self fanout: current.
			fanout isEmpty
				ifFalse: [ 
					next := policy chooseFrom: (self fanout: current).
					trace add: next.
					current := next to ]
				ifTrue: [ hasNext := false ] ].
	^ trace