running
hasNext
	
	nextElement := traceInstance detect: [ :each | each from = currentElement to ] ifNone: [ nil ].
	^nextElement isNil not