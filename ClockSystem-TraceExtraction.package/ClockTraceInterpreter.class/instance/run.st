running
run
	currentElement := traceInstance first.
	result add: currentElement.
	currentInstant := currentInstant + 1.
	[ self atEnd ]
		whileFalse: [ 
			self next.
			result add: currentElement ]