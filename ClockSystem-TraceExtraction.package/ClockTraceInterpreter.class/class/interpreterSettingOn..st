as yet unclassified
interpreterSettingOn: aBuilder
	<systemsettings>
	<clockSystem>
	(aBuilder setting: #interpretedInstances)
		parent: #ClockSystem;
		label: 'Number of instants';
		target: ClockTraceInterpreter;
		getSelector: #defaultInstants;
		setSelector: #defaultInstants:;
		notInStyle;
		default: 400;
		description: 'The maximum number of steps used to intepret a trace'