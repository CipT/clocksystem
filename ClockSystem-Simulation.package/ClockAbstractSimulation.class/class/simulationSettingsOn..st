as yet unclassified
simulationSettingsOn: aBuilder
	<systemsettings>
	<clockSystem>
	(aBuilder pickOne: #simulationClass)
		parent: #ClockSystem;
		target: ClockAbstractSimulation;
		label: 'Default Simulation Class';
		domainValues:
				({(Smalltalk at: #ClockSimulation).
					(Smalltalk at: #ClockBDDSimulation)} reject: #isNil);
		default: (Smalltalk at: #ClockSimulation).