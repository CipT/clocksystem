heuristics
runHeuristicOn: clockAssignmentSet
	^ heuristic
		ifNil: [ self randomSAT: clockAssignmentSet ]
		ifNotNil: [ self perform: heuristic with: clockAssignmentSet ]