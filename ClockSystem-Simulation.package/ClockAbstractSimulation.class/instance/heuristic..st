accessing
heuristic: aHeuristicSymbol
	heuristic := aHeuristicSymbol.
	(heuristic endsWith: ':') ifFalse: [ heuristic := heuristic, ':' ].