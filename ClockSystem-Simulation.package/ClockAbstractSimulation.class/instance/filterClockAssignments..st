exploration api
filterClockAssignments: clockAssignmentSet
	| clockValuation | 
	clockValuation := clockAssignmentSet size > 1
		ifTrue: [ 
			Array with: (self runHeuristicOn: clockAssignmentSet) ]
		ifFalse: [ 
			clockAssignmentSet
				ifEmpty: [ 
					#()
					 ]
				ifNotEmpty: [ Array with: clockAssignmentSet first ] ].
	^ clockValuation 