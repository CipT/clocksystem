exploration api
perform: activeAutomataAndFireable configuration: configuration withClocks: clockStates
	| fireablePerAutomata nEIDX toFire |
	nEIDX := activeAutomataAndFireable first.
	fireablePerAutomata := activeAutomataAndFireable second.	
	"get the first transition combination possible"
	toFire := fireablePerAutomata collect: [ :each | each first ].
	self
		execute: toFire
		configuration: configuration
		notEmptyIdx: nEIDX
		clockStates: clockStates