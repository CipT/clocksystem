heuristics
maximal: aSolution
	| max maxidx |
	max := 0.
	aSolution
		withIndexDo: [ :each :idx | 
			| v |
			v := each
				inject: 0
				into: [ :sum :e | 
					e = Tristate tick
						ifTrue: [ sum + 1 ]
						ifFalse: [ sum ] ].
			v > max
				ifTrue: [ 
					max := v.
					maxidx := idx ] ].
	^ aSolution at: maxidx