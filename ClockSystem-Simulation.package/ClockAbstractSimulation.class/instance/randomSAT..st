heuristics
randomSAT: aSolution
	"select one clockAssignment at random, pick another one if we found one that does nothing.
	note that in a given solution we cannot have two assignment with notick allover.
	hence we have at most two iteration of the following loop."
	
	| assignment | 
	[ 
	assignment := aSolution atRandom.
	(assignment reject: [ :clockState | clockState = Tristate notick ]) isEmpty ] whileTrue.
	^ assignment