heuristics
minimal: aSolution
	| min minidx |
	min := aSolution first size + 1.
	aSolution
		withIndexDo: [ :each :idx | 
			| v |
			v := each
				inject: 0
				into: [ :sum :e | 
					e = Tristate tick
						ifTrue: [ sum + 1 ]
						ifFalse: [ sum ] ].
			"exclude the points were we just do nothing"
			(v > 0 and: [ v < min ])
				ifTrue: [ 
					min := v.
					minidx := idx ] ].
	^ aSolution at: minidx