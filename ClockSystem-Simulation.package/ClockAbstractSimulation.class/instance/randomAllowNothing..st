heuristics
randomAllowNothing: aSolution
	"select one clockAssignment at random. Without removing the steps not generating a new configuration we stop here"
	
	^aSolution atRandom.
