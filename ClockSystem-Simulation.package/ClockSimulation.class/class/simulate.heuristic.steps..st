as yet unclassified
simulate: system heuristic: heuristicSymbol steps: steps
	|simulator|
	simulator := (self model: system).
	steps ifNotNil: [ simulator steps: steps ].
	simulator heuristic: heuristicSymbol.
	self inform: 'Tristate Simulation started.'.
	^simulator exploreModel 