as yet unclassified
simulate: system heuristic: heuristicSymbol steps: steps
	| solver simulator buddy trace |
	simulator := self model: system.
	steps ifNotNil: [ simulator steps: steps].
	simulator heuristic: heuristicSymbol.
	solver := ClockBuDDySatisfiability new model: simulator.
	simulator satSolver: solver.
	self inform: 'BDD Simulation started.'.
	buddy := BddH uniqueInstance.
	[ 
	| bddvars |
	buddy init: 1000 arg2: 100.
	buddy setvarnum: system clocks size.
	bddvars := (1 to: system clocks size) collect: [ :idx | buddy ithvar: idx - 1 ].
	solver
		buddy: buddy;
		bddvars: bddvars.
	trace := simulator exploreModel ]
		ensure: [ buddy done ].
	^ trace