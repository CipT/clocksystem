actions
saveFiacreGML
	| newFile |
	file ifNil: [ self notify: 'No open model'. ^nil ].
	newFile := self deriveFile: 'Save Fiacre GML' suffix: 'fcr' extension: 'gml'.
	newFile ifNil: [ ^ self ].
	FileStream
		newFileNamed: newFile
		do: [ :stream | 
			"generate the fiacre file"
			TraFiacreGraph2GML gml: self toFiacreGraph in: stream ].
		self inform: '[ClockSystem] Fiacre GML saved (', newFile name printString, ')'.