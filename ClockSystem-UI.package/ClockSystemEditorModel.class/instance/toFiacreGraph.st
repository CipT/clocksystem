actions
toFiacreGraph
	file ifNil: [ self notify: 'No open model'. ^nil ].
	fiacreGraph
		ifNil: [ 
			explorationResult ifNil: [ self exploreSystem ifNil: [ ^nil ] ].	
			"remove the internal clocks from the result"
			TraRemoveInternalClocks runOn: explorationResult.	
			"generate the fiacre graph, that generates the diamonds for the concidences"
			fiacreGraph := (TraConfigurationGraph2FiacreGraph runOn: explorationResult) ].
	^fiacreGraph