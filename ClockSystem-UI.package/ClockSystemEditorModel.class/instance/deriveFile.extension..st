private
deriveFile: aString extension: extension
	^self deriveFile: aString suffix: nil extension: extension 