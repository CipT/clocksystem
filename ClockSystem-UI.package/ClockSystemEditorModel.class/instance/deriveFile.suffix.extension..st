private
deriveFile: aString suffix: aSuffix extension: extension
	^ file
		ifNil: [ UITheme builder fileSave: aString extensions: {extension} ]
		ifNotNil: [ file parent / (file base , (aSuffix ifNil: [ '' ] ifNotNil: [ '_' , aSuffix ]) , '.' , extension) ]