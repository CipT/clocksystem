private
compileRDL
	| sys compiler |
	compiler := Smalltalk
		at: #RDLCompiler
		ifAbsent: [ ^ self notify: 'RDLCompiler class is not in the image. Load ClockSystem-RDL-Core' ].
	sys := compiler evaluate: file.
	(sys isNil or: [ (sys isKindOf: ClockSystem) not ])
		ifTrue: [ 
			self notify: 'Invalid ClockSystem model'.
			^ nil ].
	^ sys