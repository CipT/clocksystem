actions
compile
	file ifNil: [ self notify: 'No open model'. ^nil ].
	file extension = 'crd'
		ifTrue: [ system := self compileRDL ]
		ifFalse: [ system := self compileSmalltalk ].
	self inform: '[ClockSystem] compilation done'.