actions
saveIOGML
	| newFile |
	file
		ifNil: [ 
			self notify: 'No open model'.
			^ nil ].
	newFile := self deriveFile: 'Save IO GML' suffix: 'io' extension: 'gml'.
	newFile ifNil: [ ^ self ].
	explorationResult ifNil: [ self exploreSystem ifNil: [ ^ nil ] ].
	FileStream
		newFileNamed: newFile
		do: [ :stream | 
			TraRemoveInternalClocks runOn: explorationResult.
			TraConfigurationGraph2GML gml: explorationResult in: stream ].
	self inform: '[ClockSystem] I/O GML saved (' , newFile name printString , ')'