private
compileSmalltalk
	| sys |
	sys := file readStreamDo: [ :stream | Compiler evaluate: stream contents , ' system' ].
	(sys isNil or: [ (sys isKindOf: ClockSystem) not ])
		ifTrue: [ 
			self notify: 'Invalid ClockSystem model'.
			^ nil ].
	^ sys