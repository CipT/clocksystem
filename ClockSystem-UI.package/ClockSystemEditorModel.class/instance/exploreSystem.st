actions
exploreSystem
	system ifNil: [ self compile ifNil: [ ^ nil ] ].
	system ifNil: [ ^nil ].
	explorationResult := ClockParallelComposition exploreModel: system.
	fiacreGraph := nil.
	self
		inform:
			'[ClockSystem] exploration done (states: ' , explorationResult configurations size printString , ', transitions: '
				, explorationResult graph size printString , ')'