actions
saveFiacre
	| newFile |
	file ifNil: [ self notify: 'No open model'. ^nil ].
	newFile := self deriveFile: 'Save fiacre' extension: 'fcr'.
	newFile ifNil: [ ^ self ].
	FileStream
		newFileNamed: newFile
		do: [ :stream | 
			"generate the fiacre file"
			TraFiacreGraph2Fiacre fiacre: self toFiacreGraph in: stream ].
		self inform: '[ClockSystem] Fiacre saved (', newFile name printString, ')'.