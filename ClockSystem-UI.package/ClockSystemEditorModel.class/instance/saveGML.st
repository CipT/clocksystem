actions
saveGML
	| newFile |
	file
		ifNil: [ 
			self notify: 'No open model'.
			^ nil ].
	newFile := self deriveFile: 'Save GML' extension: 'gml'.
	newFile ifNil: [ ^ self ].
	explorationResult ifNil: [ self exploreSystem ifNil: [ ^ nil ] ].
	FileStream newFileNamed: newFile do: [ :stream | TraConfigurationGraph2GML gml: explorationResult in: stream ].
	self inform: '[ClockSystem] GML saved (' , newFile name printString , ')'