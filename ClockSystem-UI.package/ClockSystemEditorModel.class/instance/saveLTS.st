actions
saveLTS
	| prefix newFile |
	file
		ifNil: [ 
			self notify: 'No open model'.
			^ nil ].
	prefix := UITheme builder
		textEntry: '{sys}1'
		title: 'Enter the toplevel component of your fiacre file'
		entryText: '{sys}1'.
	newFile := self deriveFile: 'Save LTS' extension: 'lts'.
	newFile ifNil: [ ^ self ].
	FileStream newFileNamed: newFile do: [ :stream | TraFiacreGraph2Lts lts: self toFiacreGraph prefix: prefix in: stream ].
	self inform: '[ClockSystem] LTS saved (' , newFile name printString , ')'