actions
simulate
	system ifNil: [ self compile ifNil: [ ^nil ] ].
	system ifNil: [ ^nil ].
	explorationResult := ClockAbstractSimulation simulate: system heuristic: #randomSAT: "steps: 100".
	[ | trace data  smodel |
	trace := (ClockTraceExtractor from: explorationResult policy: ClockRandomPolicy new) extract.
	data := (ClockTraceInterpreter run: trace) result.
	smodel := TicksModel new
		clocks: system clocks;
		ticks: (data collect: [ :each | each vector asSet ]).
	MultiWaveView openOn: smodel waves ] value.
	self inform: '[ClockSystem] simulation done'.
	