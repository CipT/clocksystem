as yet unclassified
importIcons:  iconsList fromFolder: aString inClass: aClass category: aCategory
	iconsList
		do: [:each |
			| method form |
			form := PNGReadWriter formFromFileNamed: aString, '/', each , '.png'.

			method := each , Character cr asString ,
				'^ self icons
		at: #',each,' ifAbsentPut: [',		
				form storeString,
				']'.
			aClass class compile: method classified: aCategory ].
	aClass initialize