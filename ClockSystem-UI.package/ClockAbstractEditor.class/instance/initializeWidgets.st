initialization
initializeWidgets
	menu := MenuModel new.
	menu
		addGroup: [ :group | 
					group
						addItem: [ :item | 
							item
								name: nil;
								description: 'Open file';
								icon: Smalltalk ui icons openIcon;
								action: [ self open ] ].
					group
						addItem: [ :item | 
							item
								name: nil;
								description: 'Save file';
								icon: Smalltalk ui icons smallSaveIcon;
								action: [ self save ] ].
					group
						addItem: [ :item | 
							item
								name: nil;
								description: 'SaveAs file';
								icon: Smalltalk ui icons smallSaveAsIcon;
								action: [ self saveAs ] ] ];
		addGroup: [ :group | 
					group
						addItem: [ :item | 
							item
								name: nil;
								description: 'Inspect system';
								icon: ClockSystemIcons inspectIcon;
								action: [ self inspectSystem ] ].
					group
						addItem: [ :item | 
							item
								name: 'settings';
								description: 'Settings';
								action: [ (SettingBrowser forKeywords: self settingsSelector) open ] ] ].
	menu applyTo: self.
	code := self newText.	
	code
		acceptBlock: self acceptBlock.
	self focusOrder add: code