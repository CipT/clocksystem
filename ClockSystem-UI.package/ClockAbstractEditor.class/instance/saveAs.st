file manipulation
saveAs
	| newFile |
	newFile := self askForFileSave: 'SaveAs ClockSystem model'.
	newFile ifNil: [ ^ self ].
	
	code hasUnacceptedEdits ifTrue: [ code accept ].
	self save: code text inFile: newFile. 