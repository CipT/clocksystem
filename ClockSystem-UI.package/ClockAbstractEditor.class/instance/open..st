file manipulation
open: aFileReference
	aFileReference readStreamDo: [ :stream | code text: stream contents ].
	self model file: aFileReference