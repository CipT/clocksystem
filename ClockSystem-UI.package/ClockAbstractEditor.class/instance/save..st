file manipulation
save: string
	| file |
	file := self model file
		ifNil: [ 
			| newFile |
			newFile := self askForFileSave: 'Save ClockSystem model'.
			newFile ifNil: [ ^ self ].
			newFile ]
		ifNotNil: [ self model file ].
	self save: string inFile: file