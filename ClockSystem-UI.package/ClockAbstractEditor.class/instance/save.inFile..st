file manipulation
save: string inFile: aFile
	self model file: aFile.
	FileStream
		forceNewFileNamed: aFile
		do: [ :stream | 
			[ stream nextPutAll: string ]
				ensure: [ stream ifNotNil: [ stream close ] ] ]