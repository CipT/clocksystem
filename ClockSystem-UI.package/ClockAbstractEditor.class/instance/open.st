file manipulation
open
	| file |
	file := UITheme builder fileOpen: 'Open a ClockSystem or ClockRDL file' extensions: #('ccsl' 'crd').
	file
		ifNotNil: [ 
			self open: file fullName asFileReference ]