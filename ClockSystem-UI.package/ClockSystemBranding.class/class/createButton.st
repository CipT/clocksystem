as yet unclassified
createButton
	"delete the button if it already exists"
	iButton ifNotNil: [ iButton delete ].
	iButton := IconicButton new
		labelGraphic: ClockSystemIcons clockSystem;
		color: Color transparent;
		borderStyle: (BorderStyle width: 0 color: Color green);
		on: #mouseDown send: #open to: ClockSystemBrowser;
		position: 45 @ 45.
	iButton openInWorld