private
openModelInEditor
	tree selectedItem
		ifNotNil: [ 
			| content |
			content := tree selectedItem content.
			content isDirectory
				ifFalse: [ editor open: content asFileReference ] ]