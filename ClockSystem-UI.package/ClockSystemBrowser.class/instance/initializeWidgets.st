initialization
initializeWidgets
	menu := MenuModel new.
	menu
		addGroup: [ :group | 
			group
				addItem: [ :item | 
					item
						name: nil;
						description: 'Open directory';
						icon: ClockSystemIcons open;
						action: [ self open ] ].
			group
				addItem: [ :item | 
					item
						name: nil;
						description: 'New file';
						icon: ClockSystemIcons add;
						action: [ self newFile ] ].
			group
				addItem: [ :item | 
					item
						name: nil;
						description: 'Remove file';
						icon: ClockSystemIcons remove;
						action: [ self removeFile ] ].
			group
				addItem: [ :item | 
					item
						name: nil;
						description: 'Inspect system';
						icon: ClockSystemIcons refresh;
						action: [ tree updateTree ] ] ].
	tree := self newTree.
	tree roots: {(Smalltalk imageDirectory)}.
	tree
		childrenBlock: [ :it | 
			(it isNotNil and: [it isDirectory])
				ifTrue: [ it asFileReference entries select: [ :each | each isDirectory or: [ each extension = 'ccsl' or: [ each extension = 'crd' ] ] ] ]
				ifFalse: [ {} ] ].
	tree displayBlock: [ :it | it ifNotNil: [ it basename ] ifNil: ['']].
	tree doubleClick: [ self openModelInEditor ].
	editor := self instantiate: ClockSystemEditor