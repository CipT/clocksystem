private
newFile
	tree selectedItem
		ifNotNil: [ 
			| content |
			content := tree selectedItem content.
			content isDirectory
				ifTrue: [ 
					| name file |
					name := UITheme builder textEntry: 'file name' title: 'Enter filename' entryText: 'filename'.
					file := content asFileReference / (name).
					file exists
						ifFalse: [ 
							| stream |
							stream := file writeStream close ].
					tree updateTree ] ]