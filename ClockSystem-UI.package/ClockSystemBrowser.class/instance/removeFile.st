private
removeFile
	tree selectedItem
		ifNotNil: [ 
			| content |
			content := tree selectedItem content.
			content isDirectory
				ifFalse: [ 
					(self confirm: 'Do you want to delete?')
						ifTrue: [ content asFileReference delete ].
					tree updateTree ] ]