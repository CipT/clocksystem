as yet unclassified
defaultSpec
	<spec: #default>
	
	^ SpecLayout composed
		newRow: [ :r | 
			r newColumn: [ :c |
				c add: #menu  height: self toolbarHeight; add: #tree] width: 250;
				add: #editor];
		yourself