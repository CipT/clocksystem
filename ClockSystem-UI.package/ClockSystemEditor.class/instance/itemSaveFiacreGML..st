widgets
itemSaveFiacreGML: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Save Fiacre Graph';
				icon: ClockSystemIcons fcr_graph;
				action: [self  model saveFiacreGML ].
			menuItems add: #saveFiacreGML -> item ]