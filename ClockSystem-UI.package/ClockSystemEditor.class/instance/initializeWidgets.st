initialization
initializeWidgets
	super initializeWidgets.
	menuItems := Dictionary new.
	self menu autoRefresh: true.
	self menu
		addGroup: [ :group | 
					self itemSimulate: group.
					self itemExploreSystem: group ];
		addGroup: [ :group | 
					self itemSaveGML: group.
					self itemSaveLTS: group.
					self itemSaveFiacre: group.
					self itemSaveFiacreGML: group.
					self itemSaveIOGML: group ].
"	self disableExportButtons."