file manipulation
open: aFileReference
	aFileReference
		readStreamDo: [ :stream | 
			aFileReference extension = 'crd'
				ifTrue: [ 
					code aboutToStyle: true.
					code widget widget styler: (RDLStyler new view: code widget widget).
					code text: stream contents ]
				ifFalse: [ 
					code aboutToStyle: true.
					code widget widget styler: (code widget widget defaultStyler).
					code text: stream contents ] ].
	self model file: aFileReference