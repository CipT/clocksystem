widgets
itemSaveFiacre: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Save Fiacre';
				icon: ClockSystemIcons fiacre;
				action: [self  model saveFiacre ].
			menuItems add: #saveFiacre -> item ]