widgets
itemSaveGML: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Save GML';
				icon: ClockSystemIcons graph;
				action: [self  model saveGML ].
			menuItems add: #saveGML -> item ]