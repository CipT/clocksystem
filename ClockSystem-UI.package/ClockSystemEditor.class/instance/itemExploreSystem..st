widgets
itemExploreSystem: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Explore system';
				icon: ClockSystemIcons explore;
				action: [self  model exploreSystem ].
			menuItems add: #explore -> item ]