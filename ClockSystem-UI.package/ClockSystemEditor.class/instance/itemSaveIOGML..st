widgets
itemSaveIOGML: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Save IO Graph';
				icon: ClockSystemIcons graph_io;
				action: [self  model saveIOGML ].
			menuItems add: #saveIOGML -> item ]