widgets
itemSaveLTS: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Save LTS';
				icon: ClockSystemIcons lts;
				action: [self  model saveLTS ].
			menuItems add: #saveLTS -> item ]