widgets
itemSimulate: group
	group
		addItem: [ :item | 
			item
				name: nil;
				description: 'Simulate';
				icon: ClockSystemIcons simulate;
				action: [ self model simulate ].
			menuItems add: #simulate -> item ]