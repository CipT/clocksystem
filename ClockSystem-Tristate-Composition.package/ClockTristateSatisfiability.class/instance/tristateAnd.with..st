as yet unclassified
tristateAnd: aTristateArray with: anotherTristateArray
	| solution |
	solution := OrderedCollection new: (aTristateArray size max: anotherTristateArray size).
	aTristateArray
		do: [ :first | 
			anotherTristateArray
				do: [ :second | 
					| current |
					(current := first tristateAnd: second) "ifNil: [ first inspect. second inspect. ] "ifNotNil: [ solution add: current ] ] ].
	^ solution