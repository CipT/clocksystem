as yet unclassified
allSolutions: aConfiguration
	| solutions |
	solutions := OrderedCollection new.
	model automata
		withIndexDo: [ :automaton :idx | 
			| t |
			t := automaton transitionsInState: (aConfiguration at: idx).
			solutions := (t
				inject: Set new
				into: [ :union :transition | 
					union addAll: (self constrain: solutions with: transition in: automaton).
					union ]) asOrderedCollection.
			enabledTransitions at: idx put: t ].
	^ solutions