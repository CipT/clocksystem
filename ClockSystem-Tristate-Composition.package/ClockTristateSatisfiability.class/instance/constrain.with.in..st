as yet unclassified
constrain: solutions with: aTransition in: automaton
	| stalled localC tick notick localSolutions |
	localC := TristateArray new: model clocks size withAll: Tristate dontcare.	
	"put notick for clocks not allowed to tick"
	stalled := automaton alphabet \ aTransition vector.
	stalled do: [ :clock | localC at: clock index put: Tristate notick ].
	automaton isAlways
		ifTrue: [ 
			"The always clock ticks no matter what"
			"However the transition has to have a syncronization vector. 
			if it does not then the clock is dead. This is how the forceOne is implemented"
			aTransition vector size = 1 ifTrue: [
			localC at: (aTransition vector anyOne index) put: Tristate tick].
			localSolutions := {localC} ]
		ifFalse: [ 
			tick := localC.
			notick := localC copy.
			aTransition vector
				do: [ :clock | 
					| clockIdx |
					clockIdx := clock index.
					tick at: clockIdx put: Tristate tick.
					notick at: clockIdx put: Tristate notick ].
			localSolutions := { tick. notick } ].
	^ solutions ifEmpty: [ localSolutions ] ifNotEmpty: [ self tristateAnd: localSolutions with: solutions ]