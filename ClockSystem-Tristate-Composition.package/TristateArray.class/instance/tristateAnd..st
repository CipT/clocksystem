as yet unclassified
tristateAnd: v
	|new|
	new := TristateArray new: self size.
	self withIndexDo: [ :each :idx |
		(new at: idx put: each & (v at: idx)) ifNil: [ ^nil ].
	].
^new