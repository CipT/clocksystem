accessing
at: aSymbolOrIndex
	aSymbolOrIndex isSymbol ifTrue: [ ^(self class primitives at: aSymbolOrIndex) value: queue ].
	^queue at: aSymbolOrIndex  