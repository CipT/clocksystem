printing
printOn: aStream
	aStream nextPutAll: '{|'.
	queue do: [ :each | each printOn: aStream ].
	aStream nextPutAll: '|}'.