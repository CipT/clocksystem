as yet unclassified
primitives
	^ primitives
		ifNil: [ 
			primitives := {(#size -> [ :queue | [:args | queue size ]]).
			(#isEmpty -> [ :queue | [:args | queue isEmpty ]]).
			(#isNotEmpty -> [ :queue | [:args | queue isNotEmpty ]]).
			(#first -> [ :queue | [:args | queue first ]]).
			(#last -> [ :queue | [:args | queue last ]]).
			(#removeFirst -> [ :queue | [:args | queue removeFirst ]]).
			(#removeLast -> [ :queue | [:args | queue removeLast ]]).
			(#remove -> [ :queue | [:args | queue remove ]]).
			(#add -> [ :queue | [:args | queue add: args first ]]).
			(#addFirst -> [ :queue | [:args | queue addFirst: args first ]]).
			(#addLast -> [ :queue | [:args | queue addLast: args first ]])} asDictionary ]
		ifNotNil: [ primitives ]