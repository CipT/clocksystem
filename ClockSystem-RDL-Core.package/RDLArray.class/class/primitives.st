as yet unclassified
primitives
	^ primitives
		ifNil: [ primitives := {(#size -> [ :array | [ :args | array size ] ])} asDictionary ]
		ifNotNil: [ primitives ]