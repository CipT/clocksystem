printing
printOn: aStream
	aStream nextPut: $[.
	array do: [ :each | each printOn: aStream ].
	aStream nextPut: $].