accessing
at: aSymbolOrIndex
	aSymbolOrIndex isSymbol ifTrue: [ ^(self class primitives at: aSymbolOrIndex) value: array ].
	^array at: aSymbolOrIndex  