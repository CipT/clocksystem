explore - api
startState
	| pragmas |
	pragmas := Pragma allNamed: #RDLInitialization in: self class.
	pragmas do: [ :each | self withArgs: {} executeMethod: each method ].
	^ variables