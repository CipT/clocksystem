as yet unclassified
rdlCompilerSettingsOn: aBuilder
	<systemsettings>
	<clockSystem>
	(aBuilder setting: #rdlCompilerJavaPath)
		parent: #ClockSystem;
		label: 'Path to java executable';
		target: RDLCompiler;
		type: #FilePathEncoder;
		getSelector: #javaExePath;
		setSelector: #javaExePath:;
		notInStyle;
		default: '/usr/bin/java';
		description: 'Path to the java executable'.
		
	 (aBuilder setting: #rdlCompiler)
		parent: #ClockSystem;
		label: 'Path to ClockRDL.jar';
		target: RDLCompiler;
		type: #FilePathEncoder;
		getSelector: #clockRDLJarPath;
		setSelector: #clockRDLJarPath:;
		notInStyle;
		default: 'ClockRDL/jars/ClockRDL.jar';
		description: 'The fullpath of the ClockRDL jar'.
		