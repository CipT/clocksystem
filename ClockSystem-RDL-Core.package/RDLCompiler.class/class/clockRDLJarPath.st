as yet unclassified
clockRDLJarPath
	^ ClockRDLJarPath
		ifNil: [ ClockRDLJarPath := 'ClockRDL/jars/ClockRDL.jar' ]
		ifNotNil: [ 
			ClockRDLJarPath asFileReference exists
				ifTrue: [ ClockRDLJarPath ]
				ifFalse: [ 
					| fileStream |
					fileStream := UITheme builder fileOpen: 'Please locate ClockRDL.jar library' extensions: #(#jar).
					fileStream ifNil: [ ClockRDLJarPath := '<>' ] ifNotNil: [ ClockRDLJarPath := fileStream fullName ] ] ]