as yet unclassified
javaExePath
	^JavaExePath ifNil: [ JavaExePath := '/usr/bin/jdava' ] ifNotNil: [ 
			JavaExePath asFileReference exists
				ifTrue: [ JavaExePath ]
				ifFalse: [ 
					| fileStream |
					fileStream := UITheme builder fileOpen: 'Please locate java executable' extensions: nil.
					fileStream ifNil: [ JavaExePath := '<>' ] ifNotNil: [ JavaExePath := fileStream fullName ] ] ]