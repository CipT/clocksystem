as yet unclassified
import
	| file packages |
	self compile ifNil: [ ^ nil ].	"remove all classes from the package where we will import the code"
	packages := RPackageOrganizer default packages select: [ :p | p name = #'RDL-RelationLibrary-Test' ].
	packages ifNotEmpty: [ packages first definedClasses do: [ :classToRemove | classToRemove removeFromSystem ] ].
	file := 'generatedRDL.st' asFileReference.
	file fileIn