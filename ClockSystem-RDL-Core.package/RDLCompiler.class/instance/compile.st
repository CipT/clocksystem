as yet unclassified
compile
	| process out err errors stopSteps |
	process := ExternalOSProcess concreteClass
		programName: self class javaExePath
		arguments:
			{'-cp'.
			(self class clockRDLJarPath).
			'ClockRDL.rdl2st80.ClockRDL2Smalltalk'.
			'-libraryPath'.
			('"' , Smalltalk imageDirectory asFileReference fullName , '/"').
			'-in'.
			(infile fullName).
			'-out'.
			'generatedRDL.st'}
		initialEnvironment: OSProcess thisOSProcess environment copy.
	out := FileStream forceNewFileNamed: 'out.stream'.
	err := FileStream forceNewFileNamed: 'err.stream'.
	process
		pwd: '.' asFileReference fullName;
		initialStdOut: out;
		initialStdErr: err;
		initialize.
	process value.	
	"the following line are a hack for windows"
	stopSteps := 10.
	[ process isRunning and: [ stopSteps ~= 0 ] ]
		whileTrue: [ 
			(Delay forMilliseconds: 50) wait.
			stopSteps := stopSteps - 1 ].	
	"[ process isRunning ] whileTrue: [ (Delay forMilliseconds: 50) wait ]."
	errors := err contentsOfEntireFile.
	errors ~= ''
		ifTrue: [ 
			Transcript
				show: errors;
				open.
			^ nil ]