as yet unclassified
instance
	self import ifNil: [ ^ nil ].
	^ (Smalltalk at: #RDLRelationInstance ifAbsent: [self notify: 'ClockSystem cannot analyze ClockRDL models without a root instance'. ^nil]) root