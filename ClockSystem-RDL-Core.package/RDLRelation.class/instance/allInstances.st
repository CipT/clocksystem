accessing
allInstances 
	|result|
	result := OrderedCollection new.
	self instances do: [ :instance | result addAll: instance instances ].
	^result