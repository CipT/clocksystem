as yet unclassified
highlight: aModelString
|styler|
styler :=
	
	Styler ifNil: [Styler := PPStyler new
		parser: self new;
		addAttribute: (TextColor color: (Color darkGray)) for: 'comment';
		addAttribute: (TextColor color: (Color darkGray)) for: 'singleLineComment';
		addAttribute: TextEmphasis underlined for: 'DEF';
		addAttribute: (TextColor color:  (Color blue muchDarker)) for: 'top';
		addAttribute: (TextEmphasis bold) for: 'top';
		
		addAttribute: (TextColor color: (Color r: 0.51 g: 0.282 b: 0.573) darker) for: 'category';
		bold: 'category';
		addAttribute: TextEmphasis italic for: 'INTERNAL';

		addAttribute: (TextColor color: Color brown) for: 'formal';
		addAttribute: (TextColor color: Color green muchDarker) for: 'string';
		addAttribute: (TextColor color:  (Color blue)) for: 'number';
		addAttribute: (TextColor color:  (Color blue)) for: 'TRUE';
		addAttribute: (TextColor color:  (Color blue)) for: 'FALSE';

		bold: 'logical';
		bold: 'statement';
		addAttribute: (TextColor color: (Color r: 0.51 g: 0.282 b: 0.573) darker) for: 'SQUARES';
		bold: 'SQUARES'] ifNotNil: [ Styler ].
	^styler highlight: aModelString asText