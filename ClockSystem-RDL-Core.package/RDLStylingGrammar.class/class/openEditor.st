as yet unclassified
openEditor
	| model txtMorph window |
	model := [ :txt :mo | 
	txt isText
		ifTrue: [ 
			
			mo stylerStyled: (self highlight: txt).
			
			 ] ].
	txtMorph := PluggableTextMorph on: model text: #value:value: accept: #value:value:.
	txtMorph autoAccept:  true.
	window := SystemWindow labelled: 'FM3 Model Editor'.
	window addMorph: txtMorph frame: (0 @ 0 corner: 1 @ 1).
	window openInWorld