as yet unclassified
singleLineComment
	| end |
	end := #newline asParser.
	^ ('//' asParser, (#any asParser starLazy: end), end)