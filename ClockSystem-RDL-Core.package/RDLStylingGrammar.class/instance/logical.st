as yet unclassified
logical
	^ ((PPPredicateObjectParser anyOf: {$!. $&. $|}), $= asParser not) 
	/ ('nand' asParser, stop)
	/ ('nor' asParser, stop )
	/ ('xor' asParser, stop)