as yet unclassified
comment
	| end |
	end := '*/' asParser.
	^ ('/*' asParser, (#any asParser starLazy: end), end)