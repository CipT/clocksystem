An ExampleSDF is an implementation of SDF in CCSL as described in: 

The Clock Constraint Specification Language for building timed causality models. Application to Synchronous Data Flow graphs.
http://dx.doi.org/10.1007/s11334-009-0109-0 

Instance Variables
	idx:		<Object>
	system:		<Object>

idx
	- xxxxx

system
	- xxxxx
