sdf standard ccsl
arc: delay source: source outRate: out target: target inRate: in 
	|read write|
	read := system clock: ('read', idx printString) asSymbol .
	write := system clock: ('write', idx printString) asSymbol.
	idx := idx + 1.
	self output: source write: write  weight: out.
	self tokens: delay read: read write: write.
	self input: target read: read weight: in.