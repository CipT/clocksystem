sdf mocml
arc: size source: source outRate: out target: target inRate: in capacity: capacity
	|rchan wchan|
	rchan := system clock: ('rchan', idx printString) asSymbol.
	wchan := system clock: ('wchan', idx printString) asSymbol.
	idx := idx + 1.
	self channelSize: size read: rchan write: wchan inputRate: out outputRate: in capacity: capacity.
	source === wchan.
	target === rchan.