sdf mocml
actor: name
	| fireA runA  readA writeA|
	fireA := system clock: ('fire', name) asSymbol.
	runA  := system clock: ('run', name) asSymbol.
	readA := system clock: ('read', name) asSymbol.
	writeA := system clock: ('write', name) asSymbol.
	
	system moc: #sdf_actor
		clocks: { readA. writeA. fireA. runA. }