sdf standard ccsl
arc1: delay source: source outRate: out target: target inRate: in 
	|read write|
	read := system clock: ('read', idx printString) asSymbol.
	write := system clock: ('write', idx printString) asSymbol.
	idx := idx + 1.
	source === (write period: ({ 1 }, (0 for: (out-1))) ).
	write < (read delayFor: delay).
	(read period:  (0 for: (in-1)), {1}) < target