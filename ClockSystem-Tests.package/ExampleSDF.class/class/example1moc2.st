examples
example1moc2
	| sys a b c |
	sys := ClockSystem named: 'sdf'.

	a := sys clock: #A.
	b := sys clock: #B.
	c := sys clock: #C.

	^(self on: sys)
		arc: 0
			source: a
			outRate: 1
			target: b
			inRate: 2
			capacity: -1;
		arc: 0
			source: b
			outRate: 2
			target: c
			inRate: 1
			capacity: -1;
		arc: 2
			source: c
			outRate: 1
			target: b
			inRate: 2
			capacity: -1.