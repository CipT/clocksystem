examples
example4mocFFT4Date14_16_6_platformDualCore
	|sys a b c d e f mem  |
	sys := ClockSystem named: 'fft'.
	a := sys clock: #'Signal Provider'.
	b := sys clock: #Display1.
	c := sys clock: #NFFT.
	d := sys clock: #Threshold.
	e := sys clock: #Display2.
	f := sys clock: #AVG.
	mem := sys clock: #MEM.
	
	sys allExclusive: { a. b. c}.
	sys allExclusive: { d. e. f }.
	a ~ mem delayFor: 1.
	b ~ mem delayFor: 1.
	c ~ mem delayFor: 2.
	d ~ mem delayFor: 1.
	e ~ mem delayFor: 1.
	f ~ mem delayFor: 1. 
	
	^(self on: sys)
		edgeFrom: a to: b outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: a to: c outRate: 1 initial: 0 inRate: 16 capacity: 16;
		edgeFrom: c to: d outRate: 16 initial: 0 inRate: 4 capacity: 16;
		edgeFrom: d to: e outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: c to: f outRate: 16 initial: 16 inRate: 8 capacity: 16;
		edgeFrom: f to: c outRate: 8 initial: 0 inRate: 16 capacity: 16.