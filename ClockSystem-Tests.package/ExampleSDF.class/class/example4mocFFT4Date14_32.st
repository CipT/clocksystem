examples
example4mocFFT4Date14_32
	|sys a b c d e f|
	sys := ClockSystem named: 'fft'.
	a := sys clock: #'Signal Provider'.
	b := sys clock: #Display1.
	c := sys clock: #NFFT.
	d := sys clock: #AVG.
	e := sys clock: #'Threshold'.
	f  := sys clock: #Display2.
	^(self on: sys)
		edgeFrom: a to: b outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: a to: c outRate: 2 initial: 0 inRate: 32 capacity: 32;
		edgeFrom: c to: d outRate: 32 initial: 32 inRate: 16 capacity: 32;
		edgeFrom: d to: c outRate: 16 initial: 0 inRate: 32 capacity: 32;
		edgeFrom: c to: e outRate: 32 initial: 0 inRate: 8 capacity: 32;
		edgeFrom: e to: f outRate: 1 initial: 0 inRate: 1 capacity: 1.
		