examples
example2moc1
	| sys a b c |
	sys := ClockSystem named: 'sdf'.
	a := sys clock: #A.
	b := sys clock: #B.
	c := sys clock: #C.
	^ (self on: sys)
		arc: 0
			source: a
			outRate: 2
			target: b
			inRate: 3
			capacity: 4;
		arc: 0
			source: b
			outRate: 1
			target: c
			inRate: 2
			capacity: 2