examples
example1moc
	| sys |
	sys := ClockSystem named: 'sdf'.
	^(self on: sys)
		actor: #A;
		actor: #B;
		actor: #C;
		arc: 0
			source: sys writeA
			outRate: 1
			target: sys readB
			inRate: 2
			capacity: -1;
		arc: 0
			source: sys writeB
			outRate: 2
			target: sys readC
			inRate: 1
			capacity: -1;
		arc: 2
			source: sys writeC
			outRate: 1
			target: sys readB
			inRate: 2
			capacity: -1.