examples
example3moc
	|sys a b c d|
	sys := ClockSystem named: 'sdf'.
	a := sys clock: #A.
	b := sys clock: #B.
	c := sys clock: #C.
	d := sys clock: #D.
	^(self on: sys)
		edgeFrom: a to: b outRate: 2 initial: 0 inRate: 4 capacity: 4;
		edgeFrom: b to: c outRate: 1 initial: 0 inRate: 2 capacity: 2;
		edgeFrom: c to: d outRate: 2 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b to: d outRate: 2 initial: 0 inRate: 2 capacity: 4;
		edgeFrom: d to: a outRate: 2 initial: 4 inRate: 1 capacity: 4.
		