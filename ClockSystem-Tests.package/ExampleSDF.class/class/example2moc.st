examples
example2moc
	| sys |
	sys := ClockSystem named: 'sdf'.
	^(self on: sys)
		actor: #A;
		actor: #B;
		actor: #C;
		arc: 0
			source: sys writeA
			outRate: 2
			target: sys readB
			inRate: 3
			capacity: 4;
		arc: 0
			source: sys writeB
			outRate: 1
			target: sys readC
			inRate: 2
			capacity: 2.