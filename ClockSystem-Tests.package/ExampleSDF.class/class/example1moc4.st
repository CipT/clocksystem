examples
example1moc4
	|sys a b c|
	sys := ClockSystem named: 'sdf'.
	a := sys clock: #A.
	b := sys clock: #B.
	c := sys clock: #C.
	^(self on: sys)
		edgeFrom: a to: b outRate: 1 initial: 0 inRate: 2 capacity: 1;
		edgeFrom: b to: c outRate: 2 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: c to: b outRate: 1 initial: 2 inRate: 2 capacity: 1.
		