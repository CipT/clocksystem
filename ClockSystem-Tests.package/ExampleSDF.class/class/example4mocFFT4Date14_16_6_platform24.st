examples
example4mocFFT4Date14_16_6_platform24
	|sys a b c d e f   |
	sys := ClockSystem named: 'fft'.
	a := sys clock: #'Signal Provider'.
	b := sys clock: #Display1.
	c := sys clock: #NFFT.
	d := sys clock: #Threshold.
	e := sys clock: #Display2.
	f := sys clock: #AVG.
	
	sys allExclusive: { a. b }.
	sys allExclusive: { c. d. e. f}.

	^(self on: sys)
		edgeFrom: a to: b outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: a to: c outRate: 1 initial: 0 inRate: 16 capacity: 16;
		edgeFrom: c to: d outRate: 16 initial: 0 inRate: 4 capacity: 16;
		edgeFrom: d to: e outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edgeFrom: c to: f outRate: 16 initial: 16 inRate: 8 capacity: 16;
		edgeFrom: f to: c outRate: 8 initial: 0 inRate: 16 capacity: 16.