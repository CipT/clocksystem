examples
example5ButterflyPushPop
	|sys source b1s1 b2s1 b1s2 b2s2 drain s1 s2 s3 s4 x1 x2 x3 x4 x5 x6 x7 x8 y1 y2 y3 y4 y5 y6 y7 y8 d1 d2 d3 d4|
	sys := ClockSystem named: 'fft'.
	source := sys clock: #Source_isExecuting.
	b1s1 := sys clock: #B1S1_isExecuting.
	b2s1 := sys clock: #B2S1_isExecuting.
	b1s2 := sys clock: #B1S2_isExecuting.
	b2s2 := sys clock: #B2S2_isExecuting.
	drain  := sys clock: #Drain_isExecuting.
	
s1 := sys clock: #Source2B1S1X1_push.
s2 := sys clock: #Source2B1S1X2_push.
s3 := sys clock: #Source2B2S1X1_push.
s4 := sys clock: #Source2B2S1X2_push.

x1 := sys clock: #Source2B1S1X1_pop. 
x2 := sys clock: #Source2B1S1X2_pop. 
x3 := sys clock: #Source2B2S1X1_pop. 
x4 := sys clock: #Source2B2S1X2_pop.

y1 := sys clock: #B1S1TB1S2_push.
y2 := sys clock: #B1S1TB2S2_push.
y3 := sys clock: #B2S1TB1S2_push.
y4 := sys clock: #B2S1TB2S2_push.

x5 := sys clock: #B1S1TB1S2_pop. 
x6 := sys clock: #B1S1TB2S2_pop. 
x7 := sys clock: #B2S1TB1S2_pop. 
x8 := sys clock: #B2S1TB2S2_pop. 

y5 := sys clock: #B1S2TDY1_push.
y6 := sys clock: #B1S2TDY2_push.
y7 := sys clock: #B2S2TDY1_push.
y8 := sys clock: #B2S2TDY2_push.

d1 := sys clock: #B1S2TDY1_pop. 
d2 := sys clock: #B1S2TDY2_pop. 
d3 := sys clock: #B2S2TDY1_pop.	
d4 := sys clock: #B2S2TDY2_pop.

source ~ s1. source ~ s2. source ~ s3. source ~ s4.

x1 ~ b1s1. x2 ~ b1s1. x3 ~ b2s1. x4 ~ b2s1.
b1s1 ~ y1. b1s1 ~ y2. b2s1 ~ y3. b2s1 ~ y4.
x5 ~b1s2. x6 ~ b1s2. x7 ~ b2s2. x8 ~ b2s2.
b1s2 ~ y5. b1s2 ~ y6. b2s2 ~ y7. b2s2 ~ y8.

d1 ~ drain. d2 ~ drain. d3 ~ drain. d4 ~ drain. 
	
	^(self on: sys)
		edgeFrom: source to: b1s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b1s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b2s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b2s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		
		edgeFrom: b1s1 to: b1s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b1s1 to: b2s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s1 to: b1s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s1 to: b2s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		
		edgeFrom: b1s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b1s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2

		