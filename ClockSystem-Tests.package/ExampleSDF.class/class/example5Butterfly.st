examples
example5Butterfly
	|sys source b1s1 b2s1 b1s2 b2s2 drain|
	sys := ClockSystem named: 'fft'.
	source := sys clock: #Source_isExecuting.
	b1s1 := sys clock: #B1S1_isExecuting.
	b2s1 := sys clock: #B2S1_isExecuting.
	b1s2 := sys clock: #B1S2_isExecuting.
	b2s2 := sys clock: #B2S2_isExecuting.
	drain  := sys clock: #Drain_isExecuting.
	^(self on: sys)
		edgeFrom: source to: b1s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b1s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b2s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: source to: b2s1 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		
		edgeFrom: b1s1 to: b1s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b1s1 to: b2s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s1 to: b1s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s1 to: b2s2 outRate: 1 initial: 0 inRate: 1 capacity: 2;
		
		edgeFrom: b1s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b1s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2;
		edgeFrom: b2s2 to: drain outRate: 1 initial: 0 inRate: 1 capacity: 2

		