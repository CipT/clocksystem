examples
example1moc1
	| sys  rb rc wa wb wc |
	sys := ClockSystem named: 'sdf'.
	rb := sys clock: #readB.
	rc := sys clock: #readC.
	wa := sys clock: #A.
	wb := sys clock: #B.
	wc := sys clock: #C.
	rb === wb.
	rc === wc.
	^(self on: sys)
		arc: 0
			source: wa
			outRate: 1
			target: rb
			inRate: 2
			capacity: -1;
		arc: 0
			source: wb
			outRate: 2
			target: rc
			inRate: 1
			capacity: -1;
		arc: 2
			source: wc
			outRate: 1
			target: rb
			inRate: 2
			capacity: -1.