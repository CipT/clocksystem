test relations
testExclusion
	|system result |
	system := self system2clocks.
	system a <> system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 2).
	self assert: (result graph first  vector includesAll: {system b. }).
	self assert: (result graph second  vector includesAll: {system a. }).
