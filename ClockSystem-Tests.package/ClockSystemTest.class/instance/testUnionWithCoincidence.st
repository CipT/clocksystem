test composites
testUnionWithCoincidence
	|system result c|
	system := self system3clocks.
	c := system a union: system b.
	c === system c.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 3).
	self assert: (result graph second  vector includesAll: {system a. system b. system c. c}).
	self assert: (result graph first  vector includesAll: {system b. system c. c}).
	self assert: (result graph third vector includesAll: {system a. system c. c})
	
	
	
