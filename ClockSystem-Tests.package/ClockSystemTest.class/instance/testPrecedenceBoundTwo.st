test relations
testPrecedenceBoundTwo
	|system result |
	system := self system2clocks.
	system a preceeds: system b bound: 2.

	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 7).
	self assert: (result graph first vector includesAll: {system a. system b}).
	self assert: (result graph second vector includesAll: {system a}).
	self assert: (result graph third vector includesAll: {system b}).	
	self assert: (result graph fourth vector includesAll: {system a}).
	self assert: ((result graph at: 5) vector includesAll: {system a. system b}).
	self assert: ((result graph at: 6) vector includesAll: {system b}).
	self assert: ((result graph at: 7) vector includesAll: {system a. system b}).