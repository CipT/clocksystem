test expressions
testDelayForOne
	|system result c|
	system := self system1clock.
	c := system a delayFor: 1.
	result := system exploreModel.
	
	self assert: (result configurations size = 2).
	self assert: (result graph size = 2).
	self assert: (result graph first  vector includesAll: {system a. }).
	self assert: (result graph second vector includesAll: {system a. c }).	
