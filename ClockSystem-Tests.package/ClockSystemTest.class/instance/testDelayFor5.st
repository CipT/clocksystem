test expressions
testDelayFor5
	|system result c|
	system := self system1clock.
	c := system a delayFor: 5.
	result := system exploreModel.
	
	self assert: (result configurations size = 6).
	self assert: (result graph size = 6).
	1 to: 5 do: [ :idx | 
		self assert: ((result graph at: idx) vector includesAll: {system a. }).	
	 ].
	
	self assert: (result graph last vector includesAll: {system a. c }).	
