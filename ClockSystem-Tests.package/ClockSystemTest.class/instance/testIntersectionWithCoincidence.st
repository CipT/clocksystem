test composites
testIntersectionWithCoincidence
	|system result c|
	system := self system3clocks.
	c := system a intersection: system b.
	c === system c.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 3).
	self assert: (result graph first  vector includesAll: {system a. system b. system c. c}).
	self assert: (result graph second vector includesAll: {system b. }).
	self assert: (result graph third vector includesAll: {system a.})
	
	
	
