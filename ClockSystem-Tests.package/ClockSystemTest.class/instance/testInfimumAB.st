test expressions
testInfimumAB
	|system result c|
	system := self system2clocks.
	system a ~ system b.
	c := system a infimum: system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 3).
	self assert: (result graph first vector includesAll: {system a. c}).
	self assert: (result graph second vector includesAll: {system b}).
	self assert: (result graph third vector includesAll: {system a. c})
	
	
	
