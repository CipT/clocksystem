test only clocks
testOneClock
	|system result|
	system := self system1clock.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 1).
	self assert: (result graph first  vector includesAll: {system a.}).	
