test expressions
testSamplingOn
	|system result c|
	system := self system2clocks.
	c := system a samplingOn: system b.

	result := system exploreModel.
	
	self assert: (result configurations size = 2).
	self assert: (result graph size = 6).
	self assert: (result graph first vector includesAll: {system a}).
	self assert: (result graph second vector includesAll: {system b}).
	self assert: (result graph third vector includesAll: {system a. system b. c}).
	self assert: ((result graph at: 4) vector includesAll: {system a}).
	self assert: ((result graph at: 5) vector includesAll: {system b}).
	self assert: ((result graph at: 6) vector includesAll: {system a. system b. c})
	
	