test relations
testStrictPrecedenceBoundZero
	|system result |
	system := self system2clocks.
	system a strictlyPreceeds: system b bound: 0.

	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 0).

