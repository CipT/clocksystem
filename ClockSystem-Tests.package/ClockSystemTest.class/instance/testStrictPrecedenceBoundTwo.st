test relations
testStrictPrecedenceBoundTwo
	|system result |
	system := self system2clocks.
	system a strictlyPreceeds: system b bound: 2.

	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 6).
	self assert: (result graph first vector includesAll: {system a}).
	self assert: (result graph second vector includesAll: {system b}).	
	self assert: (result graph third vector includesAll: {system a}).
	self assert: ((result graph at: 4) vector includesAll: {system a. system b}).
	self assert: ((result graph at: 4) vector includesAll: {system b}).
	self assert: ((result graph at: 4) vector includesAll: {system a. system b}).