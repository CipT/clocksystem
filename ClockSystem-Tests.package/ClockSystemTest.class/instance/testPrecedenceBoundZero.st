test relations
testPrecedenceBoundZero
	|system result |
	system := self system2clocks.
	system a preceeds: system b bound: 0.

	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 1).
	self assert: (result graph first vector includesAll: {system a. system b}).
