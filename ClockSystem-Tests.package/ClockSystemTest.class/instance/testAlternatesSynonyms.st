test composites
testAlternatesSynonyms
	|system result c|
	system := self system2clocks.
	system a < system b.
	c := system a delayFor: 1.
	system b < c.
	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 3).
	self assert: (result graph first  vector includesAll: {system a. }).
	self assert: (result graph second vector includesAll: {system b. }).
	self assert: (result graph third vector includesAll: {system a. c})
	
	
	
