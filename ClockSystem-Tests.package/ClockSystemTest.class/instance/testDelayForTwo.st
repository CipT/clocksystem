test expressions
testDelayForTwo
	|system result c|
	system := self system1clock.
	c := system a delayFor: 2.
	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 3).
	self assert: (result graph first  vector includesAll: {system a. }).
	self assert: (result graph first  vector includesAll: {system a. }).
	self assert: (result graph third vector includesAll: {system a. c }).	
