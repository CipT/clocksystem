test expressions
testInfimumBA
	|system result c|
	system := self system2clocks.
	system b ~ system a.
	c := system a infimum: system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 3).
	self assert: (result graph first vector includesAll: {system b. c}).
	self assert: (result graph second vector includesAll: {system a}).
	self assert: (result graph third vector includesAll: {system b. c})
	
	
	
