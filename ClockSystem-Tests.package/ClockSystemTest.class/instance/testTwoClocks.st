test only clocks
testTwoClocks
	|system result|
	system := self system2clocks.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 3).
	self assert: (result graph first  vector includesAll: {system a. }).
	self assert: (result graph third vector includesAll: {system b. }).
	self assert: (result graph second vector includesAll: {system a. system b.})
	
	
	
