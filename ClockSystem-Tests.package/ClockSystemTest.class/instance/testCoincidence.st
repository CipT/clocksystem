test relations
testCoincidence
	|system result |
	system := self system2clocks.
	system a === system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 1).
	self assert: (result graph first  vector includesAll: {system a. system b}).
	
	
	
