test expression synonyms
testUnionAsPlus
	|system result c|
	system := self system2clocks.
	c := system a + system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 1).
	self assert: (result graph size = 3).
	self assert: (result graph first vector includesAll: {system a. c}).
	self assert: (result graph second vector includesAll: {system b. c}).
	self assert: (result graph third vector includesAll: {system a. system b. c})
	
	
	
