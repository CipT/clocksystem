test expressions
testFilteredByPeriod1010
	| system c ccg |
	system := self system1clock.
	c := system a filteredBy: #(() (1 0 1 0)).
	ccg := system exploreModel.
	self assert: ccg configurations size = 4.
	self assert: ccg graph size = 4.
	self assert: (ccg graph first vector includesAll: {system a. c}).
	self assert: (ccg graph second vector includesAll: {system a}).
	self assert: (ccg graph third vector includesAll: {system a. c}).
	self assert: ((ccg graph at: 4) vector includesAll: {system a}).
