test expression synonyms
testSupAB
	|system result c|
	system := self system2clocks.
	system a ~ system b.
	c := system a sup: system b.
	result := system exploreModel.
	
	self assert: (result configurations size = 3).
	self assert: (result graph size = 3).
	self assert: (result graph first vector includesAll: {system a}).
	self assert: (result graph second vector includesAll: {system b. c}).
	self assert: (result graph third vector includesAll: {system a})
	
	
	
