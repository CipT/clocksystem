as yet unclassified
actor: aSymbol
	|start execute stop|
	start := (aSymbol, '_start') asSymbol.
	execute := (aSymbol, '_isExecuting') asSymbol.
	stop :=  (aSymbol, '_stop') asSymbol.
	system
		addClocks: { execute }.
	system addInternalClocks: { start. stop }.
	system library: #MoCMLLibrary 
		relation: #sdf_actorSES
		clocks: { start. execute. stop }
		constants: { 1 }
		variables: { 0 }