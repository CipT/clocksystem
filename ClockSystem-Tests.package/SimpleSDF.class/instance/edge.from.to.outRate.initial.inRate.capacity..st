as yet unclassified
edge: aName from: outActor to: inActor outRate: outR initial: initialTokens inRate: inR capacity: capacity
	|read write|
	read := (aName, outActor,'2', inActor, '_pop') asSymbol.
	write := (aName, outActor,'2', inActor, '_push') asSymbol.
	system addClocks: { read. write }.
	system 
		moc: #sdf_channel
		clocks: { read. write }
		constants: { inR. outR. capacity }
		arguments: { initialTokens }.
	"stop causes push"
	system library: #Kernel relation: #Coincides clocks: {(outActor, '_stop') asSymbol. write}.
	"pop causes start"
	system library: #Kernel relation: #Coincides clocks: {read. (inActor, '_start') asSymbol}.