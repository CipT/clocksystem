as yet unclassified
example3
	^(self on: (ClockSystem named: 'ex2'))
		actor: #Source;
		actor: #B1S1;
		actor: #B2S1;
		actor: #B1S2;
		actor: #B2S2;
		actor: #Drain;
		edge: 'x1' from: #Source to: #B1S1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'x2' from: #Source to: #B1S1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'x1' from: #Source to: #B2S1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'x2' from: #Source to: #B2S1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		
		edge: 'y1' from: #B1S1 to: #B1S2 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y2' from: #B1S1 to: #B2S2 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y1' from: #B2S1 to: #B1S2 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y2' from: #B2S1 to: #B2S2 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		
		edge: 'y1' from: #B1S2 to: #Drain outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y2' from: #B1S2 to: #Drain outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y1' from: #B2S2 to: #Drain outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: 'y2' from: #B2S2 to: #Drain outRate: 1 initial: 0 inRate: 1 capacity: 1
		
