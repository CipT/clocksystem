as yet unclassified
example2
	^(self on: (ClockSystem named: 'ex1'))
		actor: #A;
		actor: #B;
		actor: #C;
		edge: '' from: #A to: #B outRate: 1 initial: 0 inRate: 2 capacity: 4;
		edge: '' from: #B to: #C outRate: 2 initial: 0 inRate: 1 capacity: 4;
		edge: '' from: #C to: #B outRate: 1 initial: 2 inRate: 2 capacity: 4.
