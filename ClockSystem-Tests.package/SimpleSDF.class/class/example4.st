as yet unclassified
example4
	^(self on: (ClockSystem named: 'ex3'))
		actor: #SP;
		actor: #D1;
		actor: #NFFT;
		actor: #AVG;
		actor: #TH;
		actor: #D2;
		
		edge: '' from: #SP to: #D1 outRate: 1 initial: 0 inRate: 1 capacity: 1;
		edge: '' from: #SP to: #NFFT outRate: 1 initial: 0 inRate: 16 capacity: 16;
		edge: '' from: #NFFT to: #AVG outRate: 16 initial: 16 inRate: 8 capacity: 16;
		edge: '' from: #AVG to: #NFFT outRate: 8 initial: 0 inRate: 16 capacity: 16;
		edge: '' from: #NFFT to: #TH outRate: 16 initial: 0 inRate: 4 capacity: 16;
		edge: '' from: #TH to: #D2 outRate: 1 initial: 0 inRate: 1 capacity: 1.
