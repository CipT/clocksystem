as yet unclassified
coincident: n
	| sys |
	sys := self independent: n.
	sys
		systemName: 'coincident';
		allCoincide: sys clocks asOrderedCollection.
	^ sys