as yet unclassified
myRunScript
	FileStream
		forceNewFileNamed: '../../../coincidentClocks_time.csv'
		do: [ :stream | 
			stream
				nextPutAll: 'Nb clocks; State-space size; Time';
				cr.
			1 to: 25 do: [ :n | 
				| time composition |
				time := Time
					microsecondsToRun: [ composition := ClockCartesianProductBDDExploration exploreModel: (self coincident: n) ].
				stream
					nextPutAll: n printString , ';' , composition graph size printString , '; ' , time printString;
					cr;
					flush ] ]