as yet unclassified
independent: n
	| sys |
	sys := ClockSystem named: 'independent'.
	1 to: n do: [ :i | sys clock: ('clk' , i printString) asSymbol ].
	^ sys