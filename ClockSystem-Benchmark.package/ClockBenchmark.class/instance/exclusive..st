as yet unclassified
exclusive: n
	| sys |
	sys := self independent: n.
	sys
		systemName: 'exclusive';
		allExclusive: sys clocks asOrderedCollection.
	^ sys