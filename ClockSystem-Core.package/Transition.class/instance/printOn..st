printing
printOn: aStream 	
	super printOn: aStream.
	aStream nextPut: ${.
	vector do: [:el | el printNameOn: aStream] separatedBy: [ aStream nextPutAll: '. '].
	aStream nextPut: $}.
	action ifNotNil: [ action printOn: aStream. ]
