testing
isFireableIn: automaton with: assignment
	| stalled |
	"check if the clocks that need to fire are assigned tick"
	vector
		do: [ :clock | 
			(assignment at: clock index) == Tristate tick
				ifFalse: [ ^ false ] ].	
	"check if the clock that are not allowed to fire are assigned notick"
	stalled := automaton alphabet \ vector.
	stalled
		do: [ :clock | 
			(assignment at: clock index) == Tristate notick
				ifFalse: [ ^ false ] ].
	^ true