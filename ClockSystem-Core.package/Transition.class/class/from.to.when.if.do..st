as yet unclassified
from: a to: b when: array if: predicateBlock do: actionBlock
	| i  |
	predicateBlock ifNotNil: [ ^self error: 'predicateBlock handling not implemented yet' ].
	i := self key: a value: b.
	i vector: array.
	i predicate: predicateBlock.
	i action: actionBlock.
	^ i