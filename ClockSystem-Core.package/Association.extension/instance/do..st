*ClockSystem-Core
do: aBlock
	^Transition from: self key to: self value do: aBlock.