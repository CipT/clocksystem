*ClockSystem-Core
when: array if: predicateBlock
	^Transition from: self key to: self value when: array if: predicateBlock.