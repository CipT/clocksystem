*ClockSystem-Core
if: predicateBlock do: actionBlock
	^Transition from: self key to: self value if: predicateBlock do: actionBlock.