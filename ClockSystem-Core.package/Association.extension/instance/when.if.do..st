*ClockSystem-Core
when: array if: predicateBlock do: actionBlock
	^Transition from: self key to: self value when: array if: predicateBlock do: actionBlock.