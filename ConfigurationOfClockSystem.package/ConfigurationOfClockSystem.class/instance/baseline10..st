baselines
baseline10: spec
	<version: '1.0-baseline'>

	spec for: #'common' do: [
		spec blessing: #'baseline'.
		spec repository: 'bitbucket://CipT/clocksystem.git'.
		
		spec project: 'CartesianProduct' with: [
				spec
					className: 'ConfigurationOfCartesianProduct';
					versionString: #'stable';
					repository: 'http://smalltalkhub.com/mc/CipT/CartesianProduct/main' ].
		
		spec project: 'BuDDy' with: [
				spec
					className: 'ConfigurationOfBuDDy';
					versionString: #'stable';
					repository: 'http://smalltalkhub.com/mc/CipT/BuDDy/main' ].

		spec project: 'Glamour' with: [
				spec
					className: 'ConfigurationOfGlamour';
					versionString: #'stable';
					repository: 'http://smalltalkhub.com/mc/Moose/Glamour/main' ].	
				
		spec 
			package: #'ClockSystem-Core';
			package: #'ClockSystem-Language' with: [ 
				spec requires: #('ClockSystem-Core') ];
			package: #'ClockSystem-Lib-Kernel' with: [
				spec requires: #('ClockSystem-Language')];
			package: #'ClockSystem-Composition' with: [
				spec requires: #('ClockSystem-Language' 'CartesianProduct')];

			package: #'ClockSystem-Simulation' with: [
				spec requires: #('ClockSystem-Composition')];
			package: #'ClockSystem-TraceExtraction' with: [
				spec requires: #('ClockSystem-Composition')];
			package: #'ClockSystem-Transformations' with: [
				spec requires: #('ClockSystem-Composition')];

			package: #'ClockSystem-Tristate';
			package: #'ClockSystem-BDD-Composition' with: [
				spec requires: #('ClockSystem-Composition' BuDDy 'ClockSystem-Tristate')];
			package: #'ClockSystem-Tristate-Composition' with: [
				spec requires: #('ClockSystem-Composition' 'ClockSystem-Tristate')];

			package: #'ClockSystem-Tests' with: [
				spec requires: #('ClockSystem-Composition')];
			package: #'ClockSystem-Lib-DATE14' with: [
				spec requires: #('ClockSystem-Language')];
			package: #'ClockSystem-Lib-MoCML' with: [
				spec requires: #('ClockSystem-Language')];

			package: #'ClockSystem-UI-Simulator' with: [
				spec requires: #('ClockSystem-Composition' 'Glamour')];
			package: #'ClockSystem-UI' with: [
				spec requires: #('ClockSystem-Composition' 'Glamour')].

		
		spec 
			group: 'default' with: #('Core' 'BDD' 'UI' 'Tests' 'Gemoc');
			group: 'Core' with: #('ClockSystem-Core' 'ClockSystem-Language' 
				'ClockSystem-Lib-Kernel' 'ClockSystem-Composition' 
				'ClockSystem-Simulation' 'ClockSystem-TraceExtraction' 
				'ClockSystem-Transformations' 'ClockSystem-Tristate' 
				'ClockSystem-Tristate-Composition');
			group: 'BDD' with: #('ClockSystem-BDD-Composition');
			group: 'UI' with: #('ClockSystem-UI-Simulator' 'ClockSystem-UI');
			group: 'Tests' with: #('Core' 'ClockSystem-Tests');
			group: 'Gemoc' with: #('Core' 'ClockSystem-Lib-DATE14' 'ClockSystem-Lib-MoCML'  ) ].
