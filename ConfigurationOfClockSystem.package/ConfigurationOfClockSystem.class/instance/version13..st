versions
version13: spec
	<version: '1.3' imports: #('3.0-baseline' )>

	spec for: #'common' do: [
		spec blessing: #'release'.
		spec description: 'filetree repo path support'.
		spec author: 'CiprianTeodorov'.
		spec timestamp: '16/4/2015 17:39'.
 ].
