baselines
baseline20: spec
	<version: '2.0-baseline'>
	spec
		for: #common
		do: [ 
			spec blessing: #baseline.
			GitRepoPath
				ifNotNil: [ spec repository: 'filetree://' , GitRepoPath ]
				ifNil: [ spec repository: 'bitbucket://CipT/clocksystem.git' ].
			spec
				project: 'CartesianProduct'
				with: [ 
					spec
						className: 'ConfigurationOfCartesianProduct';
						versionString: #stable;
						repository: 'http://smalltalkhub.com/mc/CipT/CartesianProduct/main' ].
			spec
				project: 'BuDDy'
				with: [ 
					spec
						className: 'ConfigurationOfBuDDy';
						versionString: #stable;
						repository: 'http://smalltalkhub.com/mc/CipT/BuDDy/main' ].
			spec
				project: 'SMark'
				with: [ 
					spec
						className: 'ConfigurationOfSMark';
						versionString: #stable;
						repository: 'http://smalltalkhub.com/mc/StefanMarr/SMark/main/' ].
			spec
				project: 'ClassicBench'
				with: [ 
					spec
						className: #ConfigurationOfClassicBench;
						versionString: #bleedingEdge;
						repository: 'http://smalltalkhub.com/mc/ClementBera/classic-bench/main/' ].
			spec
				package: #'ClockSystem-Core';
				package: #'ClockSystem-Language' with: [ spec requires: #('ClockSystem-Core') ];
				package: #'ClockSystem-Lib-Kernel' with: [ spec requires: #('ClockSystem-Language') ];
				package: #'ClockSystem-Composition' with: [ spec requires: #('ClockSystem-Language' 'CartesianProduct') ];
				package: #'ClockSystem-Simulation' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-TraceExtraction' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-Transformations' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-Tristate';
				package: #'ClockSystem-BDD-Composition'
					with: [ spec requires: #('ClockSystem-Composition' #BuDDy 'ClockSystem-Tristate') ];
				package: #'ClockSystem-Tristate-Composition'
					with: [ spec requires: #('ClockSystem-Composition' 'ClockSystem-Tristate') ];
				package: #'ClockSystem-Tests' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-Lib-DATE14' with: [ spec requires: #('ClockSystem-Language') ];
				package: #'ClockSystem-Lib-MoCML' with: [ spec requires: #('ClockSystem-Language') ];
				package: #'ClockSystem-UI-Simulator' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-UI' with: [ spec requires: #('ClockSystem-Composition') ];
				package: #'ClockSystem-Benchmark' with: [ spec requires: #('ClockSystem-Lib-Kernel' 'ClockSystem-BDD-Composition' 'SMark' 'ClassicBench') ].
			spec
				group: 'default' with: #('Core' 'BDD' 'UI' 'Tests' 'Gemoc' 'Benchmark');
				group: 'Core'
					with:
						#('ClockSystem-Core' 'ClockSystem-Language' 'ClockSystem-Lib-Kernel' 'ClockSystem-Composition' 'ClockSystem-Simulation' 'ClockSystem-TraceExtraction' 'ClockSystem-Transformations' 'ClockSystem-Tristate' 'ClockSystem-Tristate-Composition');
				group: 'BDD' with: #('ClockSystem-BDD-Composition');
				group: 'UI' with: #('ClockSystem-UI-Simulator' 'ClockSystem-UI');
				group: 'Tests' with: #('Core' 'ClockSystem-Tests');
				group: 'Gemoc' with: #('Core' 'ClockSystem-Lib-DATE14' 'ClockSystem-Lib-MoCML');
				group: 'Benchmark' with: #('ClockSystem-Benchmark') ]