versions
version20: spec
	<version: '2.0' imports: #('4.0-baseline' )>

	spec for: #'common' do: [
		spec blessing: #'release'.
		spec description: 'ClockRDL integration'.
		spec author: 'CiprianTeodorov'.
		spec timestamp: '01/11/2015 19:25'.
 ].
