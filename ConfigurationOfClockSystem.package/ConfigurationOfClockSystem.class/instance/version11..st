versions
version11: spec
	<version: '1.1' imports: #('1.0-baseline' )>

	spec for: #'common' do: [
		spec blessing: #'release'.
		spec description: 'initial version'.
		spec author: 'CiprianTeodorov'.
		spec timestamp: '11/7/2014 12:01'.

		spec 
			project: 'Glamour' with: '2.6-snapshot';
			package: #'ClockSystem-Core' with: 'ClockSystem-Core-CiprianTeodorov.7';
			package: #'ClockSystem-Language' with: 'ClockSystem-Language-CiprianTeodorov.18';
			package: #'ClockSystem-Lib-Kernel' with: 'ClockSystem-Lib-Kernel-CiprianTeodorov.9';
			package: #'ClockSystem-Composition' with: 'ClockSystem-Composition-CiprianTeodorov.20';
			package: #'ClockSystem-Simulation' with: 'ClockSystem-Simulation-CiprianTeodorov.11';
			package: #'ClockSystem-TraceExtraction' with: 'ClockSystem-TraceExtraction-CiprianTeodorov.3';
			package: #'ClockSystem-Transformations' with: 'ClockSystem-Transformations-CiprianTeodorov.5';
			package: #'ClockSystem-Tristate' with: 'ClockSystem-Tristate-CiprianTeodorov.1';
			package: #'ClockSystem-BDD-Composition' with: 'ClockSystem-BDD-Composition-CiprianTeodorov.6';
			package: #'ClockSystem-Tristate-Composition' with: 'ClockSystem-Tristate-Composition-CiprianTeodorov.1';
			package: #'ClockSystem-Tests' with: 'ClockSystem-Tests-CiprianTeodorov.9';
			package: #'ClockSystem-Lib-DATE14' with: 'ClockSystem-Lib-DATE14-CiprianTeodorov.4';
			package: #'ClockSystem-Lib-MoCML' with: 'ClockSystem-Lib-MoCML-CiprianTeodorov.4';
			package: #'ClockSystem-UI-Simulator' with: 'ClockSystem-UI-Simulator-CiprianTeodorov.3';
			package: #'ClockSystem-UI' with: 'ClockSystem-UI-CiprianTeodorov.2';
			package: #'ClockSystem-Benchmark' with: 'ClockSystem-Benchmark-CiprianTeodorov.1'. ].
