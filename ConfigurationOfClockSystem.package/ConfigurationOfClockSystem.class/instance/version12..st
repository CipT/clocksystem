versions
version12: spec
	<version: '1.2' imports: #('2.0-baseline' )>

	spec for: #'common' do: [
		spec blessing: #'release'.
		spec description: 'filetree repo path support'.
		spec author: 'CiprianTeodorov'.
		spec timestamp: '16/4/2015 17:39'.

		spec 
			project: 'Glamour' with: '2.6-snapshot' ].
