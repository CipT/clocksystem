as yet unclassified
openOn: model
	|scene|
	scene := self new model: model.
	^ AthensSceneView new
		scene: scene;
		openInWindow.