as yet unclassified
renderOn: aCanvas
	| start gap current path morph |
	aCanvas setPaint: Color black.
	morph := model asTextMorph.
	morph
		position: position;
		drawOnAthensCanvas: aCanvas.
	start := 0.
	current := position + (100 @ 22).
	gap := 10.
	aCanvas setPaint: Color green.
	path := aCanvas
		createPath: [ :builder | 
			builder
				absolute;
				moveTo: current.
			model contents
				doWithIndex: [ :each :idx | 
					each = start
						ifTrue: [ 
							current := (current x + gap) @ current y.
							builder lineTo: current.
							builder moveTo: current ]
						ifFalse: [ 
							each = 0
								ifTrue: [ 
									current := current x @ (current y + 20).
									builder lineTo: current.
									builder moveTo: current.
									current := (current x + gap) @ current y.
									builder lineTo: current.
									builder moveTo: current ]
								ifFalse: [ 
									current := current x @ (current y - 20).
									builder lineTo: current.
									builder moveTo: current.
									current := (current x + gap) @ current y.
									builder lineTo: current.
									builder moveTo: current ] ].
					start := each ] ].
	aCanvas setStrokePaint: (Color green ).
	aCanvas drawShape: path