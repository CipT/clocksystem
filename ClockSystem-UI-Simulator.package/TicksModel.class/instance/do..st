accessing
do: aBlock
	self clocks
		do: [ :clock | 
			| values |
			values := self ticks
				withIndexCollect: [ :tickingclocks :idx | 
					(tickingclocks asSet includes: clock)
						ifTrue: [ 1 ]
						ifFalse: [ 0 ] ].
			aBlock value: clock clockName value: values ]