accessing
waves
	| waves |
	waves := OrderedCollection new.
	self do: [ :clock :data | waves add: (Wave named: clock data: data) ].
	^ waves