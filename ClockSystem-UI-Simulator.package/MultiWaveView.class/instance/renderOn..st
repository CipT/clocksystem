as yet unclassified
renderOn: aCanvas
	model
		doWithIndex: [ :each :idx | 
			WaveView new
				model: each;
				position: 0 @ (idx * 40);
				renderOn: aCanvas ]