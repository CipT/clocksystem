as yet unclassified
ccg:  aCCG in: aFile

	"remove the internal clocks from the result"
	TraRemoveInternalClocks runOn: aCCG.
	"generate the fiacre graph, that generates the diamonds for the concidences"
	fcrGraph := TraConfigurationGraph2FiacreGraph runOn: aCCG.
	"generate the fiacre file"
	TraFiacreGraph2Fiacre fiacre: fcrGraph in: aFile.
	ccg := aCCG.