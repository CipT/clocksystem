as yet unclassified
system: system in: aFile

	"explore the CCSL model"
	ccg := system exploreModel.
	"remove the internal clocks from the result"
	TraRemoveInternalClocks runOn: ccg.
	"generate the fiacre graph, that generates the diamonds for the concidences"
	fcrGraph := TraConfigurationGraph2FiacreGraph runOn: ccg.
	"generate the fiacre file"
	TraFiacreGraph2Fiacre fiacre: fcrGraph in: aFile