api
runOn: aCCG

	aCCG graph do:[:each | 
		each vector: (each vector reject: [ :clock | (clock expression isNil not) or: [ clock internal ] ])
	].
