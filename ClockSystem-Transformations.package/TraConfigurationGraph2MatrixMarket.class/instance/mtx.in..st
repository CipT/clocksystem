as yet unclassified
mtx: aGraph in: aFileOrStream
	(aFileOrStream isKindOf: Stream)
		ifTrue: [ self toMTX: aGraph value in: aFileOrStream ]
		ifFalse: [ 
			| stream |
			[ 
			stream := FileStream newFileNamed: aFileOrStream.
			self toMTX: aGraph value in: stream ]
				ensure: [ stream ifNotNil: [ stream close ] ] ]
