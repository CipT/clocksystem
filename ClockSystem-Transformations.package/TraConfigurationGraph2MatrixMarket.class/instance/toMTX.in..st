as yet unclassified
toMTX: aCCG in: aStream
	| idx conf2id |
	aStream
		nextPutAll: '%%MatrixMarket matrix coordinate pattern general';
		cr.
	aCCG graph size = 0
		ifFalse: [ aStream nextPutAll: aCCG configurations size printString, ' ' , aCCG configurations size printString, ' ' , aCCG graph size printString; cr].
	idx := 0.
	conf2id := Dictionary new.
"	conf2id at: aCCG initial put: idx."
	aCCG configurations do: [ :each | conf2id at: each ifAbsentPut: (idx := idx + 1) ].
	aCCG graph
		do: [ :each | 
			aStream
				nextPutAll: (conf2id at: each from) printString , ' ' , (conf2id at: each to) printString;
				cr ]