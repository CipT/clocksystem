as yet unclassified
toIndices: aTrace on: aCCG in: aStream
	
	aStream
		nextPutAll: '%%MatrixMarket matrix coordinate pattern general';
		cr.
	aCCG graph size = 0
		ifFalse: [ aStream nextPutAll: aTrace configurations size printString, ' ' , aTrace configurations size printString, ' ' , aTrace graph size printString; cr].
	
	aTrace graph
		do: [ :each | 
			aStream
				nextPutAll: (conf2id at: each from) printString , ' ' , (conf2id at: each to) printString;
				cr ]