as yet unclassified
index: aTrace on: aGraph in: aFile
	| stream |
	[ 
	stream := FileStream newFileNamed: aFile , '_all.mtx'.
	self toMTX: aGraph value in: stream ]
		ensure: [ stream ifNotNil: [ stream close ] ].
	[ 
	stream := FileStream newFileNamed: aFile , '_trace.mtx'.
	self toIndices: aTrace on: aGraph value in: stream ]
		ensure: [ stream ifNotNil: [ stream close ] ]