api
runOn: aCCG
	| ccg conf2id idx |
	ccg := ClockConfigurationGraph new.
	ccg system: aCCG system.
	idx := 0.
	conf2id := Dictionary new.
	ccg initial: (conf2id at: aCCG initial put: idx).
	ccg
		configurations:
			(aCCG configurations
				collect: [ :each | 
					| id |
					id := conf2id at: each ifAbsentPut: (idx := idx + 1).
					id ]).
	ccg
		graph:
			(aCCG graph
				collect: [ :transition | (conf2id at: transition from) -> (conf2id at: transition to) when: transition vector ]).
	^ ccg