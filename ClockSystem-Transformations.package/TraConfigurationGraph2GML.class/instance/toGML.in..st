as yet unclassified
toGML: aCCG in: aStream
	| idx conf2id |
	aStream
		nextPutAll: 'graph [ directed 1 id 42 label "clock configuration graph"';
		cr.
	aCCG graph size = 0
		ifFalse: [ 
			aStream
				tab;
				nextPutAll:
						'node [ id 0 label "0" graphics [ center [ x 82.0000 y 42.0000 ] w 16.0000 h 16.0000 type "circle" fill "#000000" ]]' ].
	idx := 0.
	conf2id := Dictionary new.
	aCCG configurations
		do: [ :each | 
			conf2id at: each put: (idx := idx + 1).
			aStream
				tab;
				nextPutAll: 'node [ id ' , (conf2id at: each) printString , ' label "' , (idx := idx + 1) printString , '" ]';
				cr ].
	aCCG graph size = 0
		ifFalse: [ 
			aStream
				tab;
				nextPutAll: 'edge [ source 0 target ' , (conf2id at: aCCG initial) printString , ' label ""]';
				cr.
			aCCG graph
				do: [ :each | 
					aStream
						tab;
						nextPutAll:
								'edge [ source ' , (conf2id at: each from) printString , ' target ' , (conf2id at: each to) printString , ' label "'.
					aStream nextPut: ${.
					each vector do: [ :clock | aStream nextPutAll: clock clockName ] separatedBy: [aStream nextPut: $.].
					aStream nextPut: $}.
					aStream
						nextPutAll: '"]';
						cr ] ].
	aStream nextPutAll: ']'