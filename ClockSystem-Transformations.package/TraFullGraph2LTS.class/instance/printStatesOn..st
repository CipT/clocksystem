printing
printStatesOn: aStream
	| states |
	states := ccg configurations asArray.
	"serialize state names"
	states doWithIndex: [ :each :id | 
		aStream nextPutAll: 's', id printString, ' '.
		state2id at: each put: (id-1). ].
	"serialize the id of the initial state"
	aStream cr; nextPutAll: (state2id at: ccg initial) printString; cr.
	"serialize the contents of all configurations"
	states do: [ :each | 
		(each flatCollect: [:v |v ]) do:[:e | e printOn: aStream. aStream space. ].
		aStream cr.].