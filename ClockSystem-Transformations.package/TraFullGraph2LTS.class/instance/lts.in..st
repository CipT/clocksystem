printing
lts: aCCGraph in: aFileOrStream
	ccg := aCCGraph.
	(aFileOrStream isKindOf: Stream)
		ifTrue: [ self printLtsOn: aFileOrStream ]
		ifFalse: [ 
			| stream |
			[ 
			stream := FileStream newFileNamed: aFileOrStream.
			self printLtsOn: stream ]
				ensure: [ stream ifNotNil: [ stream close ] ] ]