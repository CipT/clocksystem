printing
printLtsOn: aStream 
	
	ccg system clocks size printOn: aStream.
	aStream space.
	ccg configurations size printOn: aStream.
	aStream space.
	ccg graph size printOn: aStream.
	aStream space.
	(ccg initial flatCollect: [:each | each]) size printOn: aStream.
	aStream cr.
	
	self printClocksOn: aStream.
	self printStatesOn: aStream.
	self printTransitionsOn: aStream.
