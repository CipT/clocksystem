printing
printTransitionsOn: aStream
	ccg graph
		do: [ :transition | 
			(self stateIdOf: transition from) printOn: aStream.
			aStream space.
			(transition vector collect: [ :each | (self clockIdOf: each) ]) do: [:each | aStream print: each ] separatedBy: [ aStream nextPut: $  ]. 
			aStream space.
			(self stateIdOf: transition to) printOn: aStream.
			aStream cr. ]