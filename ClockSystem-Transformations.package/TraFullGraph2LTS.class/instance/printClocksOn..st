printing
printClocksOn: aStream
	| clocks |
	clocks := ccg system clocks asArray.
	"serialize the clock names"
	clocks doWithIndex: [ :each :id | 
		aStream nextPutAll: each clockName, ' '.
		clock2id at: each put: (id-1). ].
	aStream cr.