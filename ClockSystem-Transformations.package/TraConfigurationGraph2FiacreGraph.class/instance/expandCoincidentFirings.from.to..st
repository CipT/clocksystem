initialize-release
expandCoincidentFirings: firings from: source to: target
	| current |
	firings
		permutationsDo: [ :order | 
			current := source.
			order
				do: [ :clock | 
					| adj to isLast|
					isLast := clock = order last.
					adj := graph at: current ifAbsentPut: [ Set new ].
					to := (adj
						detect: [ :each | 
							each value = clock
								ifTrue: [ 
									isLast
										ifTrue: [ each key asString beginsWith: 's' ]
										ifFalse: [ each key asString beginsWith: 'i' ] ]
								ifFalse: [ false ] ]
						ifNone: [ 
							(isLast
								ifTrue: [ target ]
								ifFalse: [ self newState ]) -> nil ]) key.
					adj add: to -> clock.
					current := to ] ]