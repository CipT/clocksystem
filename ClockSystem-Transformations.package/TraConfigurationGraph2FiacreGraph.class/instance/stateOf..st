accessing
stateOf: aConfig

	^configMap at: aConfig ifAbsent: [ self error: 'configuration not registered' ]