accessing
registerConfigurations: configurations
	configurations
		do: [ :configuration | configMap at: configuration ifAbsentPut: [ ('s' , (idx := idx + 1) printString) asSymbol ] ]