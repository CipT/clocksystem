initialize-release
extractGraph: aCCG
	configMap at: aCCG initial put: 's0'.
	self registerConfigurations: aCCG configurations.
	aCCG graph
		do: [ :each | 
			| adjacencies from to |
			from := self stateOf: each from.
			to := self stateOf: each to.
			adjacencies := graph at: from ifAbsentPut: [ Set new ].
			each vector size = 1
				ifTrue: [ adjacencies add: to -> each vector first ]
				ifFalse: [ 
					each vector isEmpty ifTrue: [ adjacencies add: to->nil ]
					ifFalse: [ 
					self expandCoincidentFirings: each vector from: from to: to ] ] ].
	^aCCG system clocks -> graph