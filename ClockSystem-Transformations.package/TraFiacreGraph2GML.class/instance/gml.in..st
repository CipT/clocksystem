as yet unclassified
gml: aGraph in: aFileOrStream

	(aFileOrStream isKindOf: Stream)
		ifTrue: [ self toGML: aGraph value in: aFileOrStream ]
		ifFalse: [ 
			| stream |
			[ 
			stream := FileStream newFileNamed: aFileOrStream.
			self toGML: aGraph value in: stream ]
				ensure: [ stream ifNotNil: [ stream close ] ] ]
