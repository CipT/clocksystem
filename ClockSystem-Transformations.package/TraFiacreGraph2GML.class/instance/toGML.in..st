as yet unclassified
toGML: aGraph in: aStream
	| nodes idx|
	aStream
		nextPutAll: 'graph [ directed 1 id 42 label "clock configuration graph -- fiacre"';
		cr.
	aGraph size = 0
		ifFalse: [ 
			aStream
				tab;
				nextPutAll:
						'node [ id 0 label "0" graphics [ center [ x 82.0000 y 42.0000 ] w 16.0000 h 16.0000 type "circle" fill "#000000" ]]'].
	
	nodes := Dictionary new.
	idx := 0.
	aGraph keys
		do: [ :each | 
			nodes at: each put: (idx := idx + 1).
			aStream
				crtab;
				nextPutAll: 'node [ id ' , idx printString, ' label "' , each , '"]' ].
	aGraph size = 0 ifFalse: [ 
	aStream
				tab;
				nextPutAll: 'edge [ source 0 target ' , (nodes at: #s0) printString , ' label ""]';
				cr]. 
	aGraph
		keysAndValuesDo: [ :source :destinations | 
			destinations
				do: [ :each | 
					| target |
					target := each key.
					nodes at: target
						ifAbsent: [ 
							nodes at: target put: (idx := idx + 1).
							aStream
								crtab;
								nextPutAll: 'node [ id ' , idx printString , ' label "' , target , '"]' ].
					aStream
						crtab;
						nextPutAll:
								'edge [ source ' , (nodes at: source) printString , ' target ' , (nodes at: target) printString , ' label "' , (each value ifNil: [ '' ] ifNotNil: [ each value clockName ]), '"]' ] ]