as yet unclassified
printFiacreOn: aStream
	| transitionStream | 
	transitionStream := String new writeStream.
	self printTransitionsOn: transitionStream.
	self printProcessOn: aStream.
	self printStateSetOn: aStream.
	aStream
		crtab;
		nextPutAll: 'init to s0'.
	aStream nextPutAll: transitionStream contents