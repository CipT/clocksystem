accessing
printStateSetOn: aStream
	aStream tab; nextPutAll: 'states '.
	states
		do: [ :s | aStream nextPutAll: s ]
		separatedBy: [ 
			aStream
				nextPut: $,;
				space ].
	aStream cr