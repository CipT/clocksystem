as yet unclassified
fiacre: aFcrGraph in: aFileOrStream
	self fcrGraph: aFcrGraph.
	(aFileOrStream isKindOf: Stream)
		ifTrue: [ self printFiacreOn: aFileOrStream ]
		ifFalse: [ 
			| stream |
			[ 
			stream := FileStream newFileNamed: aFileOrStream.
			self printFiacreOn: stream ]
				ensure: [ stream ifNotNil: [ stream close ] ] ]