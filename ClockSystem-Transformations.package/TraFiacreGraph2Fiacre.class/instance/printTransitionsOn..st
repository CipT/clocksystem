accessing
printTransitionsOn: aStream
	fcrGraph value
		keysAndValuesDo: [ :start :targets | 
			states add: start.
			aStream
				crtab;
				nextPutAll: 'from ' , start.
			aStream crtab: 2.
			targets size = 1
				ifTrue: [ 
					aStream
						nextPutAll: targets anyOne value clockName , '; to ' , (states add: targets anyOne key);
						crtab: 2 ]
				ifFalse: [ 
					aStream
						nextPutAll: 'select';
						crtab: 3.
					targets
						do: [ :target | 
							aStream
								nextPutAll: target value clockName , '; to ' , (states add: target key);
								crtab: 2 ]
						separatedBy: [ 
							aStream
								nextPutAll: '[]';
								tab ].
					aStream
						nextPutAll: 'end select' ] ]