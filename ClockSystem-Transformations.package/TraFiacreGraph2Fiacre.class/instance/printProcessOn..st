as yet unclassified
printProcessOn: aStream
	aStream nextPutAll: 'process ccsl ['.
	(fcrGraph key asArray select: [ :clock | clock expression isNil ])
		do: [ :clock | clock printNameOn: aStream ]
		separatedBy: [ 
			aStream
				nextPut: $,;
				space ].
	aStream
		nextPutAll: ' : out none] is';
		cr