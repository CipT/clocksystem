printing
printTransitionsOn: aStream
	fcrGraph value
		keysAndValuesDo: [ :start :targets | 
			| from to |
			from := self stateIdOf: start.
			targets
				do: [ :target | 
					nTransitions := nTransitions + 1.
					to := self stateIdOf: target key.
					aStream
						cr;
						nextPutAll: from printString , ' '.
					target value
						ifNotNil: [ aStream nextPutAll: (prefix ifNil: [ '' ] ifNotNil: [ prefix , ':' ]) , target value clockName ].
					aStream nextPutAll: ' ' , to printString ] ]