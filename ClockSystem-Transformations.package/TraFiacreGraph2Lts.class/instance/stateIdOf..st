accessing
stateIdOf: aStateName

	^states at: aStateName ifAbsentPut: [ (aStateName copyFrom: 2 to: aStateName size) asInteger ]