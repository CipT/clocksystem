printing
printStatesOn: aStream
	| sortedStates |
	sortedStates := states associations asSortedCollection: [ :a :b | a value < b value ].
	sortedStates do: [ :each | aStream nextPutAll: each key , ' ' ]