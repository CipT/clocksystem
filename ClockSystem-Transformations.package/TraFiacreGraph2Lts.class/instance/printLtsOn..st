printing
printLtsOn: aStream 
	|transitionStream| 
	transitionStream := String new writeStream.
	self printTransitionsOn: transitionStream.
	aStream nextPutAll: states size printString, ' ', nTransitions printString; cr.
	self printStatesOn: aStream.
	aStream 
		nextPutAll: transitionStream contents 