api
fanin: aState in: aSetOfTransitions

	^aSetOfTransitions select: [ :transition | transition to = aState ]