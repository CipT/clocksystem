api
fanout: aState in: aSetOfTransitions

	^aSetOfTransitions select: [ :transition | transition from = aState ]