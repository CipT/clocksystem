api
runOn: aCCG
	| graph hasEmpty current configurations|
	hasEmpty := true.
	current := aCCG graph.
	[ hasEmpty ]
		whileTrue: [ 
			hasEmpty := false.
			graph := OrderedCollection new.
			current
				do: [ :transition | 
					transition vector isEmpty
						ifTrue: [ 
							hasEmpty := true.
							(self fanout: transition to in: current)
								do: [ :fanout | 
									| tran |
									graph add: (tran := transition from -> fanout to when: fanout vector) ] ]
						ifFalse: [ graph add: transition ] ].
			current := graph ].
	graph := OrderedCollection new.
	configurations := Set new.
	current asSet
		do: [ :transition | 
			transition from = aCCG initial
				ifFalse: [ (self fanin: transition from in: current) ifNotEmpty: [ graph add: transition.
						configurations add: transition from.
						configurations add: transition to ] ]
				ifTrue: [ graph add: transition.
					configurations add: transition from.
					configurations add: transition to ] ].
	aCCG configurations: configurations.
	aCCG graph: graph