*RDL-Core
fromRDL: aRDLRelationInstance
	| rdlRelations rdlClocks|
	rdlRelations := aRDLRelationInstance allInstances.
	rdlClocks := OrderedCollection new.
	rdlRelations do: [ :relation | rdlClocks addAll: relation clocks ].
	(rdlClocks select: [ :each | each isNil ]) ifNotEmpty: [ ^self error: 'Some clocks are not mapped' ].
	rdlClocks do: [ :each | each system: self ].
	self clocks: rdlClocks.
	self relations addAll: rdlRelations.