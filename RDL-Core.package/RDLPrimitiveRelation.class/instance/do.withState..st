explore - api
do: aTransition withState: aState
	|block|
	block := aTransition action.
	block ifNotNil: [ 
		variables := aState.
		block value ].
