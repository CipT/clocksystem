comparing
hash
	|hash|
	hash := self species hash.
	hash := (hash + array hash) hashMultiply.
	^hash
	