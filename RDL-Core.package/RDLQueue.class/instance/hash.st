comparing
hash
	|hash|
	hash := self species hash.
	hash := (hash + queue hash) hashMultiply.
	^hash
	