initialization
initialize
	super initialize.
	primitives := {(#size -> [ :args | queue size ]).
	(#isEmpty -> [:args | queue isEmpty]).
	(#isNotEmpty -> [:args | queue isNotEmpty ]).
	(#first -> [:args | queue first]).
	(#last -> [:args | queue last]).
	(#removeFirst -> [:args | queue removeFirst]).
	(#removeLast -> [:args | queue removeLast]).
	(#remove -> [:args | queue remove]).
	(#add -> [:args | queue add: args first]).
	(#addFirst -> [:args | queue addFirst: args first]).
	(#addLast -> [:args | queue addLast: args first]).
	} asDictionary 