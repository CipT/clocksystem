as yet unclassified
handler: solutions
	^ Bddallsathandler
		on: [ :varset :size | 
			| arr sols tick notick dontcare|
			tick := Tristate tick. notick := Tristate notick. dontcare := Tristate dontcare.
			arr := Array new: model clocks size.
			sols := OrderedCollection with: arr.
			1 to: size do: [ :idx | 
				| value state trueS falseS |
				value := varset byteAt: idx - 1.
				value < 0
					ifTrue: [ state := dontcare ]
					ifFalse: [ 
						value = 0
							ifTrue: [ state := notick ]
							ifFalse: [ state := tick ] ].
				state == dontcare
					ifTrue: [ "expand dont cares in true/false branches"
						trueS := sols.
						falseS := sols collect: [ :each | each copy ].
						trueS do: [ :each | each at: idx put: tick ].
						falseS do: [ :each | each at: idx put: notick ].
						sols := trueS , falseS ]
					ifFalse: [ sols do: [ :each | each at: idx put: state ] ]	"arr at: idx put: state." ].
			solutions addAll: sols	"arr asArray" ]