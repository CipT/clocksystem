as yet unclassified
bdd4automaton: automaton transitions: transitions
	^ transitions
		inject: buddy false
		into: [ :bdd :transition | buddy addref: (buddy or: bdd arg2: (self constraintFor: transition inAutomaton: automaton)) ]