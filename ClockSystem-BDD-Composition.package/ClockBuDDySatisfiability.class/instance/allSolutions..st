as yet unclassified
allSolutions: aConfiguration
	| solutions stateBDD |
	"it should be possible to initialize only once the BDD package and the vars used.
	then each time this function is called we create a new bdd using the same variables.
	if you do that don't forget to call buddy done at the end of the exploration"
	solutions := OrderedCollection new.
	[ 
	stateBDD := buddy true.
	model automata
		withIndexDo: [ :automaton :idx | 
			| transitions |
			transitions := automaton transitionsInState: (aConfiguration at: idx).
			stateBDD := buddy addref: (buddy and: stateBDD arg2: (self bdd4automaton: automaton transitions: transitions)).
			enabledTransitions at: idx put: transitions ].
	buddy allsat: stateBDD handler: (self handler: solutions) ]
		ensure: [ buddy delref: stateBDD ].
	^ solutions