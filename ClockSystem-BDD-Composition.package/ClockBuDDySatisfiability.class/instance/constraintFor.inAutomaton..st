as yet unclassified
constraintFor: aTransition inAutomaton: automaton
	| localBDD stalled |
	"put notick for clocks not allowed to tick"
	stalled := automaton alphabet \ aTransition vector.
	localBDD := stalled
		inject: buddy true
		into: [ :bdd :stalledClock | buddy addref: (buddy and: bdd arg2: (buddy not: (bddvars at: stalledClock index))) ].
	automaton isAlways
		ifTrue: [ 
			"The always clock ticks no matter what"
			"However the transition has to have a syncronization vector. 
			if it does not then the clock is dead. This is how the forceOne is implemented"
			aTransition vector size = 1
				ifTrue: [ localBDD := buddy addref: (buddy and: localBDD arg2: (bddvars at: aTransition vector anyOne index)) ] ]
		ifFalse: [ 
			aTransition vector size > 1
				ifTrue: [ 
					| coincidenceBDDs |
					coincidenceBDDs := aTransition vector
						inject: {nil. nil}
						into: [ :bdd :clock | 
							| bddvar |
							bddvar := bddvars at: clock index.
							bdd first
								ifNil: [ 
									{ bddvar. (buddy not: bddvar) } ]
								ifNotNil: [ 
									{(buddy addref: (buddy and: bdd first arg2: bddvar)).
									(buddy addref: (buddy and: bdd second arg2: (buddy not: bddvar)))} ] ].
					localBDD := buddy
						addref: (buddy and: localBDD arg2: (buddy or: coincidenceBDDs first arg2: coincidenceBDDs second)) ] ].
	^ localBDD