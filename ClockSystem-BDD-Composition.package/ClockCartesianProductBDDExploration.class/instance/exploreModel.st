as yet unclassified
exploreModel
	buddy := BddH uniqueInstance.
	self initializeExploration.
	[ 
	buddy init: 1000 arg2: 100.
	buddy setvarnum: clocks size.
	bddvars := (1 to: clocks size) collect: [ :idx | buddy ithvar: idx - 1 ].
	[ self atEnd ] whileFalse: [ self next ].
	graph configurations: known ]
		ensure: [ buddy done ].
	^ graph