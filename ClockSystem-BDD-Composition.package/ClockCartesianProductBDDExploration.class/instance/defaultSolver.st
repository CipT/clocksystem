as yet unclassified
defaultSolver
	^ ClockBuDDySatisfiability new
		model: self;
		buddy: buddy;
		bddvars: bddvars