A ClockConcatenation is representing the sequential composition of constraints.

It contains an array of expression ClockRelations from which  one is considered the current one.
The first element of the state is the index of the current expression. Then by convention the second element is the state of the expression itself.

Its alphabet contains the death clock.

When added to the system it should somehow enable the current expression automaton, and the associated coincidence automaton.

Instance Variables
	coincidenceRelation:		a coincidence relation that is added between the Concatenation clock and the current expression clock
	deathClock:		a clock unique to each concatenation that is used to perform the death transitions

coincidenceRelation
	- xxxxx

deathClock
	- xxxxx
