as yet unclassified
transitionsInState: aState
	| aidx childState |
	aidx := aState first.
	aidx > automata size ifTrue: [ ^{} ]. "the last clock in the concatenation died"
	childState := aState second.
	^ ((automata at: aidx) finalStates includes: childState)
		ifTrue: [ { aidx -> (aidx + 1) when: { self deathClock } } ]
		ifFalse: [ (self automata at: aidx) valueWithArguments: (aState copyFrom: 2 to: aState size) , constants , clocks ]