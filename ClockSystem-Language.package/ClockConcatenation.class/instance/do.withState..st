as yet unclassified
do: aTransition withState: aState
	| block forwardState |
	aTransition vector first == self deathClock
		ifTrue: [ |aidx|
			aidx := aState at: 1 put: aState first + 1.
			aState at: 2 put: 0.
			coincidenceRelation clocks at: 2 put: (automata at: aidx) clocks last. ]
		ifFalse: [ 
			block := aTransition action.
			forwardState := aState deepCopy
				removeFirst;
				yourself.
			block ifNotNil: [ block value: forwardState ].
			forwardState atWrap: 1 put: aTransition target.
			aState
				replaceFrom: 2
				to: aState size
				with: forwardState
				startingAt: 1 ]