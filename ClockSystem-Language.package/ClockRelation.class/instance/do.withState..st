explore - api
do: aTransition withState: aState
	|block|
	block := aTransition action.
	block ifNotNil: [ block value: aState ].
	aState atWrap: 1 put: aTransition target.