printing
printExpressionOn: aStream
	aStream nextPutAll: 'library: #', libraryName. 
	aStream nextPutAll: ' expression: #', relationName , ' clocks: {'.
	(1 to: clocks size - 1) do: [ :each | (clocks at: each) printNameOn: aStream ] separatedBy: [aStream nextPutAll: '. '].
	aStream nextPut: $}.
	constants
		ifNotEmpty: [ 
			aStream nextPutAll: ' constants: '.
			constants printOn: aStream ].
	variables
		ifNotEmpty: [ 
			aStream nextPutAll: ' variables: '.
			variables printOn: aStream ]