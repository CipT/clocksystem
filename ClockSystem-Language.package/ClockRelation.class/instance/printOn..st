printing
printOn: aStream
	aStream nextPutAll: 'library: #', libraryName asString.
	self isExpression
		ifTrue: [ aStream nextPutAll: ' expression: #' , relationName , ' clocks: {' ]
		ifFalse: [ aStream nextPutAll: ' relation: #' , relationName , ' clocks: {' ].
	clocks do: [ :each | each printNameOn: aStream ] separatedBy: [ aStream nextPutAll: '. ' ].
	aStream nextPut: $}.
	constants
		ifNotEmpty: [ 
			aStream nextPutAll: ' constants: '.
			constants printOn: aStream ].
	variables
		ifNotEmpty: [ 
			aStream nextPutAll: ' variables: '.
			variables printOn: aStream ]