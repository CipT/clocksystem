as yet unclassified
clockSystemSettingsOn: aBuilder
	<systemsettings>
	<clockSystem>
	(aBuilder group: #ClockSystem)
		label: 'ClockSystem';
		with: [ 
					(Smalltalk at: #ClockParallelComposition ifAbsent: [ nil ])
						ifNotNil: [ 
							(aBuilder pickOne: #compositionClass)
								target: (Smalltalk at: #ClockParallelComposition);
								label: 'Default Composition Strategy';
								domainValues:
										({(Smalltalk at: #ClockCartesianProductExploration).
											(Smalltalk at: #ClockCartesianProductBDDExploration)} reject: #isNil);
								default: (Smalltalk at: #ClockCartesianProductExploration ifAbsent: [nil]). ].
					]