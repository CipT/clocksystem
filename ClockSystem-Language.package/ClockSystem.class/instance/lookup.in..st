private
lookup: aRelationName in: aLibraryName
	"first try to lookup in the local library"
	aLibraryName isSymbol ifTrue: [ 
		"one level library"
		self library at: aLibraryName ifPresent: [ :lib | lib at: aRelationName ifPresent: [:relation | ^relation ] ].
		
		Smalltalk at: aLibraryName ifPresent: [ :libClass | (libClass respondsTo: aRelationName) ifTrue: [ ^libClass perform: aRelationName ] ] ].
	
	aLibraryName isArray ifTrue: [ |lib|
		lib := self hierarchicalLookup: aLibraryName.
		lib ifNotNil: [ lib at: aRelationName ifPresent: [ :relation | ^relation ] ] ].
	
	aLibraryName isClass ifTrue: [ 
		(aLibraryName respondsTo: aRelationName ) ifTrue: [ ^aLibraryName perform: aRelationName ] ].
	
	^self error: 'The relation ', aRelationName , ' is not present in library ', aLibraryName printString.
	