api
clockNamed: aSymbol
	| result |
	result := clocks select: [ :each | each clockName = aSymbol ].
	result ifEmpty: [ ^ nil ].
	result size > 1
		ifTrue: [ ^ self error: 'multiple clocks with the same name' ].
	^ result anyOne