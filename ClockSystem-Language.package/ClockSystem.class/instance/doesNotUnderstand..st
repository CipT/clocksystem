error handling
doesNotUnderstand: aMessage
	aMessage arguments
		ifEmpty: [ 
			| clock |
			clock := self clockNamed: aMessage selector.
			^ clock ifNil: [ super doesNotUnderstand: aMessage ] ifNotNil: [ clock ] ].
	^ super doesNotUnderstand: aMessage