api
clock: aSymbol
	|clk|
	((self class selectorsWithArgs: 0) includes: aSymbol) ifTrue: [ ^self error: aSymbol printString, ' is a reserved word. Name your clock differently' ].
	clk := Clock new
		clockName: aSymbol asSymbol ;
		system: self.
	(clocks includes: clk) ifTrue: [ ^self error: 'Clock named ', clk clockName, ' already defined' ].
	clocks add: clk.
	unconstrainedClocks add: clk.
	^clk