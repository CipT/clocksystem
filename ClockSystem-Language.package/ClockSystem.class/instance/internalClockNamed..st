api
internalClockNamed: aSymbol
	|clk|
	clk := Clock new
		clockName: aSymbol;
		system: self;
		internal: true.
	(clocks includes: clk) ifTrue: [ ^self error: 'Clock named ', clk clockName, ' already defined' ].
	clocks add: clk.
	"since internal clocks are used in expressions they are always constrained.
	thus we don't need to add them to the unconstrained set"
	^clk