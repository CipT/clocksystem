expression - api
library: aLibName expression: sym clocks: clks constants: constants variables: vars
	| eClock exp |  
	clks last isSymbol
		ifTrue: [ 
			eClock := self clockNamed: clks last.
			clks at: (clks size) put: (eClock) ]
		ifFalse: [ eClock := clks last ].
	eClock internal
		ifFalse: [ ^ self error: 'The clock ' , eClock clockName printString , ' should be an internal clock' ].
	exp := self
		library: aLibName
		relation: sym
		clocks: clks
		constants: constants
		variables: vars.
	exp isExpression: true.
	^eClock expression: exp