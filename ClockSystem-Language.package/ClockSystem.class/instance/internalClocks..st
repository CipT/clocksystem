api
internalClocks: aListOfSymbolsOrClocks
	clocks addAll: (aListOfSymbolsOrClocks
		collect: [ :each | 
			each isSymbol
				ifTrue: [ self internalClockNamed: each ]
				ifFalse: [ each ] ]) asSet 