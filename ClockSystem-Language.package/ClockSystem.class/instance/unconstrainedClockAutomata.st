accessing
unconstrainedClockAutomata
	^ unconstrainedClocks asOrderedCollection
		collect: [ :clk | 
			ClockRelation new
				relationName: #tick;
				automata: Clock tick;
				clocks: {clk};
				constants: {};
				variables: {};
				system: self ]