printing
printOn: aStream 
	aStream nextPutAll: 'ClockSystem ', systemName; cr.
	self clocks do:[:each | each printOn: aStream. aStream cr].
	self relations do:[:each | each printOn: aStream. aStream cr].
	