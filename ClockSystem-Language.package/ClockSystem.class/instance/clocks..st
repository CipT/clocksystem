api
clocks: aListOfSymbolsOrClocks
	clocks addAll: (aListOfSymbolsOrClocks
		collect: [ :each | 
			each isSymbol
				ifTrue: [ self clock: each ]
				ifFalse: [ each ] ]) asSet 