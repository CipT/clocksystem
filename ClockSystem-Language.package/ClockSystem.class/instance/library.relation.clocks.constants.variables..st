relation - api
library: aRelationLibrary relation: sym clocks: clks constants: constants variables: vars
	| re automata constrainedClocks |
	"library := aRelationLibrary isSymbol ifTrue: [ Smalltalk at: aRelationLibrary ] ifFalse: [ aRelationLibrary  ].
	automata := library perform: sym.	"
	automata := self lookup: sym in: aRelationLibrary. 
	
	"name resolution for clocks. Retrieves the clock from the clocks list if just a symbol is given"	
	constrainedClocks := clks
		collect: [ :clockOrSymbol | 
			clockOrSymbol isSymbol
				ifTrue: [ |clk| clk := self clockNamed: clockOrSymbol. clk ifNil: [ self error: 'Clock ', clockOrSymbol printString, ' undefined'. ] ]
				ifFalse: [ clockOrSymbol ] ].
	constrainedClocks do: [ :clk | unconstrainedClocks remove: clk ifAbsent: [  ] ].
	re := ClockRelation new
		libraryName: aRelationLibrary;
		relationName: sym;
		automata: automata;
		clocks: constrainedClocks;
		constants: constants;
		variables: vars;
		system: self.
	self relations add: re.
	^ re