private
hierarchicalLookup: aLibraryName
	^ aLibraryName
		inject: self library
		into: [ :currentLibrary :currentName | 
			currentName = #'.'
				ifTrue: [ currentLibrary ]
				ifFalse: [ currentLibrary at: currentName ifPresent: [ :p | p ] ifAbsent: [ ^ nil ] ] ]