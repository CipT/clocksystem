printing
printOn: aStream
	aStream nextPutAll: clockName.
	expression
		ifNotNil: [ 
			aStream nextPutAll: ' := '.
			expression printExpressionOn: aStream ].
	aStream nextPut: $.