automata
tick
	"A clock not constrained needs an automaton to make it tick sometimes"
	^[:s :c |
		{ s->s when: { c } } ]