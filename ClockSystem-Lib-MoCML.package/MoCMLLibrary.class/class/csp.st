csp
csp
	"This is a clock that encodes the COMETA CSP MoC"
	^ [ :s :r :w :ok | 
	s = 0
		ifTrue: [ 
			{ 0 -> 1 when: {r. w} } ]
		ifFalse: [ 
			{ 1 -> 0 when: { ok } }
			] ]