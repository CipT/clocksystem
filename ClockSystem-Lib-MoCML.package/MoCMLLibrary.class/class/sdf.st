sdf
sdf
	" 1 state (0 -- initial state), n variables, m constants, k clocks"
	^[:s :size :inRate :outRate :capacity :r :w  | 
		s caseOf: { 
			[ 0 ] -> [ |transitions|
						transitions := OrderedCollection new.
						size >= inRate ifTrue: [ 
							transitions add: (0->0 when: { r } do: [:configuration | 
								|sz| 
								sz := configuration at: 2.
								configuration at: 2 put: (sz - inRate) ]) ].
						(capacity < 0 or: [capacity - size >= outRate]) ifTrue: [ 
							transitions add: ((0->0) when: { w } do: [:configuration |
								|sz| 
								sz := configuration at: 2.
								configuration at: 2 put: (sz + outRate) ]) ].
						transitions asArray.
					].
		} ]