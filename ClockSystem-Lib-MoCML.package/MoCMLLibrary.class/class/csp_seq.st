csp
csp_seq
^ [ :s :visibleP :internalP :endP :visibleQ :internalQ :endQ | 
	s caseOf: { 
	[ 0 ] -> [{ 0->0 when: {visibleP}. 0->0 when: { internalP }. 0->1 when: { endP } } ].
	[ 1 ] -> [ { 1->1 when: { visibleQ }. 1->1 when: { internalQ }. 1->2 when: { endQ } } ].
	[ 2 ] -> [ {  } ]
	} ]