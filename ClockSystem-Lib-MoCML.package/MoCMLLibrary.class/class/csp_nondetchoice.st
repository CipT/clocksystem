csp
csp_nondetchoice
^ [ :s :visibleP :internalP :endP :visibleQ :internalQ :endQ | 
	s caseOf: { 
	[ 0 ] -> [{  0->1 when: { internalP }.
			    0->2 when: { internalQ } } ].
	[ 1 ] -> [ { 1->1 when: { visibleP }.  1->1 when: { internalP }. 1->3 when: { endP } } ].
	[ 2 ] -> [ { 2->2 when: { visibleQ }. 2->2 when: { internalQ }. 2->3 when: { endQ } } ].
	[ 3 ] -> [ {  } ].
	} ]