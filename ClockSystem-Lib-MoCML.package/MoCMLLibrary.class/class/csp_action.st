csp
csp_action
^ [ :s :action :endP | 
	s caseOf: { 
	[ 0 ] -> [{  0->1 when: { action } } ].
	[ 1 ] -> [ { 1->2 when: { endP } } ].
	[ 2 ] -> [ { } ].
	} ]