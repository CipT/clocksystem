sdf
sdf_actor
	" 1 state (0 -- initial state), n variables, m constants, k clocks"
	^[:s :read :write :fire :run | 
		s caseOf: { 
			[ 0 ] -> [ { 0 -> 1 when: { fire }. } ].
			[ 1 ] -> [ { 1 -> 2 when:  { read } } ].
			[ 2 ] -> [ { 2 -> 3 when: { run } } ].
			[ 3 ] -> [ { 3 -> 0 when: { write } } ]
		} ]