*ClockSystem-Lib-MoCML
rdv: write ack: ackClock
	"This is a clock that encodes the COMETA CSP MoC
	It synchronizes self with anotherClock and enables the ack clock when they are synchronized
	"
	system cspRead: self write: write ack: ackClock
