as yet unclassified
explore: aClockSystem resultIn: resultsPath
	| runtime result rccg fiacreGraph ltsFile fcrGmlFile gmlFile resultFile sysName mtxFile |
	[ 
	runtime := Time millisecondsToRun: [ result := ClockCartesianProductExploration exploreModel: aClockSystem ].
	resultsPath := '/Users/ciprian/Playfield/GeMOC/gemoc-obp-bridge/'.
	sysName := aClockSystem systemName.	"export MTX"
	mtxFile := resultsPath , sysName , '.mtx'.
	FileStream forceNewFileNamed: mtxFile do: [ :stream | TraConfigurationGraph2MatrixMarket mtx: result in: stream ].
	rccg := TraConfigurationGraph2LTS runOn: result.
	TraRemoveInternalClocks runOn: rccg.
	TraRemoveInternalEvents runOn: rccg.
	fiacreGraph := TraConfigurationGraph2FiacreGraph runOn: rccg.	"export LTS"
	ltsFile := resultsPath , sysName , '.lts'.
	FileStream forceNewFileNamed: ltsFile do: [ :stream | TraFiacreGraph2Lts lts: fiacreGraph prefix: '{sys}1' in: stream ].	"export GML with coincidence expanded"
	fcrGmlFile := resultsPath , sysName , '_fcr.gml'.
	FileStream forceNewFileNamed: fcrGmlFile do: [ :stream | TraFiacreGraph2GML gml: fiacreGraph in: stream ].	"export GML without coincidence expanded"
	gmlFile := resultsPath , sysName , '.gml'.
	FileStream forceNewFileNamed: gmlFile do: [ :stream | TraConfigurationGraph2GML gml: rccg in: stream ].	"export GML full"
	gmlFile := resultsPath , sysName , '_full.gml'.
	FileStream forceNewFileNamed: gmlFile do: [ :stream | TraConfigurationGraph2GML gml: result in: stream ].
	resultFile := resultsPath , sysName , '_csys.results'.
	FileStream
		forceNewFileNamed: resultFile
		do: [ :stream | 
			stream
				nextPutAll: 'system ' , sysName;
				cr.
			stream
				nextPutAll: 'states: ' , result configurations size printString;
				cr.
			stream
				nextPutAll: 'transitions: ' , result graph size printString;
				cr.
			stream
				nextPutAll: 'time: ' , runtime printString , ' ms';
				cr ] ]
		on: Exception do: [ 
			resultFile := resultsPath , sysName , '_csys.results'.
			FileStream
				forceNewFileNamed: resultFile
				do: [ :stream | 
					stream
						nextPutAll: 'system ' , sysName;
						cr;
						nextPutAll: 'ERROR' ] ]