*ClockSystem-Lib-MoCML
sdfRead: r rate: rRate write: w rate: wRate capacity: cap initial: size
	| arguments constants myClocks |
	constants := {rRate. wRate. cap}.
	arguments := {size}.
	myClocks := {r. w}.
	self
		moc: #sdf
		clocks: myClocks
		constants: constants
		arguments: arguments