*ClockSystem-Lib-MoCML
cspRead: r write: w ack: ok
	| myClocks |
	myClocks := {r. w. ok}.
	self
		moc: #csp
		clocks: myClocks
