*ClockSystem-Lib-MoCML
sdfRead: r rate: rRate write: w rate: wRate initial: size
	| arguments constants myClocks |
	constants := {rRate. wRate. -1}.
	arguments := {size}.
	myClocks := {r. w}.
	self
		moc: #sdf
		clocks: myClocks
		constants: constants
		arguments: arguments