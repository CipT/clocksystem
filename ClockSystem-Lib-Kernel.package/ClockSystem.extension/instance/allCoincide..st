*ClockSystem-Lib-Kernel-multi-relations
allCoincide: someClocks 
	1 to: someClocks size do: [ :i |
		i+1 to: someClocks size do: [ :j |
			|	lhs rhs	|
			lhs := someClocks  at: i.
			rhs := someClocks at: j.
			
			lhs isSymbol ifTrue: [ lhs := self clockNamed: lhs ].
			rhs isSymbol ifTrue: [ rhs := self clockNamed: rhs ].
			
			lhs === rhs.
			
		 ]
	]