*ClockSystem-Lib-Kernel-multi-relations
allUnion: someClocks named: aSymbol
	| current constrainedClocks |
	someClocks size < 1
		ifTrue: [ ^ self error: 'union needs at least one clock' ].
	someClocks size < 2
		ifTrue: [ ^ self error: 'union with only one clock is not supported yet' ].
	someClocks size = 2
		ifTrue: [ 
			^ self
				library: ClockKernelLibrary
				expression: #union
				clocks:
					{(someClocks first).
					(someClocks last).
					aSymbol} ].
		
	constrainedClocks := someClocks
		collect: [ :clockOrSymbol | 
			clockOrSymbol isSymbol
				ifTrue: [ |clk| 
					clk := self clockNamed: clockOrSymbol. 
					clk ifNil: [ self error: 'Clock ', clockOrSymbol printString, ' undefined'. ] ]
				ifFalse: [ clockOrSymbol ] ].		
		
	current := constrainedClocks first union: constrainedClocks second.
	3 to: constrainedClocks size - 1 do: [ :i | current := current union: (constrainedClocks at: i) ].
	^ self
		library: ClockKernelLibrary
		expression: #union
		clocks:
			{current.
			(constrainedClocks last).
			aSymbol}