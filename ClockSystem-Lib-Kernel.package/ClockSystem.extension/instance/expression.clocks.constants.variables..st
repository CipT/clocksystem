*ClockSystem-Lib-Kernel-expressions
expression: sym clocks: clks constants: constants variables: vars
"	| eClock exp |
	eClock := self internalClock: sym.
	exp := self
		relation: sym
		clocks: clks , {eClock}
		constants: constants
		variables: vars.
	eClock expression: exp.
	^ eClock"
	^self library: ClockKernelLibrary expression: sym clocks: clks, { self internalClock: sym } constants: constants variables: vars