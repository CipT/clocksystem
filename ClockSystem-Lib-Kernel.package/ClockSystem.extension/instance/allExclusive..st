*ClockSystem-Lib-Kernel-multi-relations
allExclusive: someClocks 
	1 to: someClocks size do: [ :i |
		i+1 to: someClocks size do: [ :j |
			(someClocks at: i) <> (someClocks at: j).	
		 ]
	]