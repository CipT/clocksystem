*ClockSystem-Lib-Kernel-expressions
sampling: aBaseClock
	^ self system
		expression: #sampling
		clocks:
			{self.
			aBaseClock}