*ClockSystem-Lib-Kernel-expressions
samplingOn: aBaseClock
	^ self system
		expression: #samplingOn
		clocks:
			{self.
			aBaseClock}