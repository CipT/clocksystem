*ClockSystem-Lib-Kernel-expressions
upTo: aClock
	^ self system
		expression: #preemption
		clocks:
			{self.
			aClock}