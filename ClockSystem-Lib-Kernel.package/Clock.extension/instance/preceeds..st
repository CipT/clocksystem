*ClockSystem-Lib-Kernel-relations
preceeds: anotherClock
	"unbounded precedence"

	self system
		relation: #precedence
		clocks:
			{self.
			anotherClock}
		constants: {-1}