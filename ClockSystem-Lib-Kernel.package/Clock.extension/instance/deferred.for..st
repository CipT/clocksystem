*ClockSystem-Lib-Kernel-expressions
deferred: aClock for: anArray
	^ self system
		expression: #deferred
		clocks:
			{self.
			aClock}
		variables: {anArray}