*ClockSystem-Lib-Kernel-expressions
delayFor: aNumber
	^ self system expression: #delay clocks: {self} variables: {aNumber}