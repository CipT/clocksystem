*ClockSystem-Lib-Kernel-relations
preceeds: anotherClock bound: k
	"bounded precedence"

	self system
		relation: #precedence
		clocks:
			{self.
			anotherClock}
		constants: {k}