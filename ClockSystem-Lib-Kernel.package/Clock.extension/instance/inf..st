*ClockSystem-Lib-Kernel-expressions synonyms
inf: b
	(b isKindOf: Collection) ifTrue: [ |x|
		b size = 1 ifTrue: [ ^self infimum: b first ].
		x := b asOrderedCollection.
		^(self inf: (x removeFirst)) inf: x.
	 ].
	^self infimum: b