*ClockSystem-Lib-Kernel-relations
strictlyPreceeds: anotherClock
	"unbounded strict precedence"

	self system
		relation: #strictPrecedence
		clocks:
			{self.
			anotherClock}
		constants: {-1}