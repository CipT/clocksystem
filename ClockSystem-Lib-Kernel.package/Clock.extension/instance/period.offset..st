*ClockSystem-Lib-Kernel-expressions synonyms
period: period offset: offset
	|periodBW offsetBW| 
	period isInteger ifTrue: [ periodBW := #(1), ((2 to: period) collect: [ :e | 0 ])  ]
		ifFalse: [ periodBW := period ].
	offset isInteger ifTrue: [ offsetBW := (1 to: offset) collect: [ :e | 0 ]  ]
		ifFalse: [ offsetBW := offset ].
	^self filteredBy: { offsetBW.  periodBW}