*ClockSystem-Lib-Kernel-expressions
strictSampling: aBaseClock
	^ self system
		expression: #strictSampling
		clocks:
			{self.
			aBaseClock}