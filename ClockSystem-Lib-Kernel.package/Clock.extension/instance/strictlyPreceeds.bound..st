*ClockSystem-Lib-Kernel-relations
strictlyPreceeds: anotherClock bound: k
	"bounded strict precedence"

	self system
		relation: #strictPrecedence
		clocks:
			{self.
			anotherClock}
		constants: {k}