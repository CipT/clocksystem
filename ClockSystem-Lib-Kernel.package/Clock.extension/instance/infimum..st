*ClockSystem-Lib-Kernel-expressions
infimum: aClock
	^ self system
		expression: #infimum
		clocks:
			{self.
			aClock}