*ClockSystem-Lib-Kernel-expressions
supremum: aClock
	^ self system
		expression: #supremum
		clocks:
			{self.
			aClock}