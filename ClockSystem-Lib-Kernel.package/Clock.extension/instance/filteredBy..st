*ClockSystem-Lib-Kernel-expressions
filteredBy: twoBinaryWords
	|offset binaryword|
	binaryword := twoBinaryWords first, twoBinaryWords second.
	offset := twoBinaryWords first size.
	^ self system expression: #filtering clocks: {self} constants: { binaryword. offset }.