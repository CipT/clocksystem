*ClockSystem-Lib-Kernel-expressions
concat: aClock
	^ self system
		expression: #concatenation
		clocks:
			{self.
			aClock}