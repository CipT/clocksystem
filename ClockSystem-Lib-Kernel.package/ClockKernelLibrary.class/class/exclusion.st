ccsl - relations - sts
exclusion
	^[:s :a :b |  
		{ s -> s when: { a }. 
			s -> s when: { b } } ].