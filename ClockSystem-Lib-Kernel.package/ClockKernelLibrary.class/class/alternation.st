ccsl - relations - sts
alternation
	^ [ :s :a :b | 
	s = 0
		ifTrue: [ { 0 -> 1 when: {a} } ]
		ifFalse: [ { 1 -> 0 when: {b} } ] ]