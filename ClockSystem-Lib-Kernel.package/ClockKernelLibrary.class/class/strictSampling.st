ccsl - expressions - sts
strictSampling
	"the sampling ticks in coincidence with the tick of the base clock immediately following a tick of the trigger clock, and then dies. The sampling tick is not allowed to be coincident with the trigger"
		"DIES"
	^[:s :trigger :baseClock :sampled |
		s = 0 ifTrue: [ { s->s when: { baseClock }.
			s->(s+1) when: { trigger } } ]
		ifFalse: [ 
			s = 1 ifTrue: [ 
				{ s -> s when: { trigger }.
				s ->(s+1) when: { baseClock. sampled } 	"DIES"}
				 ]
			ifFalse: [ 
			{ s->s when: { trigger. baseClock }.
			s->s when: { trigger }.
			s->s when: { baseClock } } ]]
	]