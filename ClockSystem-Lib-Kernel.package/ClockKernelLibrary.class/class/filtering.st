ccsl - expressions - sts
filtering
	^ [ :s :binaryWord :offset :baseClock :filtered | 
	| whoTicks |
	whoTicks := (s+1 <= binaryWord size and: [ (binaryWord at: s+1) = 1 ])
		ifTrue: [ 
			{baseClock. filtered} ]
		ifFalse: [ {baseClock} ].
	(s+1) <= offset
		ifTrue: [ { s -> (s + 1) when: whoTicks } ]
		ifFalse: [ 
			| newS |
			newS := (offset = binaryWord size and: [ (s+1) > offset ])
				ifTrue: [ s ]
				ifFalse: [ 
					(s+1) = binaryWord size
						ifTrue: [ offset ]
						ifFalse: [ s + 1 ] ].
			{ s -> newS when: whoTicks } ] ]