ccsl - expressions - sts
supremum
	^[ :s :a :b :c |
	s = 0 
		ifTrue: [ 
			{ s->s when: { a. b. c }.
			s->(s+1) when: { a }.
			s->(s-1) when: { b } } ]
		ifFalse: [ 
			s > 0 
				ifTrue: [ 
					{ s->s when: { a. b. c }.
					s->(s+1) when: { a }.
					s->(s-1) when: { b. c } } ]
				ifFalse: [ 
					{ s->s when: { a. b. c }.
					s->(s+1) when: { a. c }.
					s->(s-1) when: { b } } ] ] ].