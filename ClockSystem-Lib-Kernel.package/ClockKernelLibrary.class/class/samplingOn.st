ccsl - expressions - sts
samplingOn
	^[:s :trigger :base :sampled | 
		s = 0 
			ifTrue: [ 
				{ 0->0 when: { base }.
				0->0 when: { base. trigger. sampled }.
				0->1 when: { trigger } } ]
			ifFalse: [ 
				{ 1->1 when: { trigger }.
				  1->1 when: { base. trigger. sampled }.
				  1->0 when: { base. sampled } } ]
	].