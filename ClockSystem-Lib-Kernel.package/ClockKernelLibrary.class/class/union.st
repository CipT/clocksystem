ccsl - expressions - sts
union
	^[:s :a :b :c |
		{ s->s when: { a. c }.
		s->s when: { b. c }.
		s->s when: { a. b. c } }
	]
	