ccsl - expressions - sts
forceOne
	"DIES"
	^[:s :c |
		s = 0 
			ifTrue: [ { s->1 when: { c } } ]
			ifFalse: [ { "s->s+1 when: { die }" } ] ]