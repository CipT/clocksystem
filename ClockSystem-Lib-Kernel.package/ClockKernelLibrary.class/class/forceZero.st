ccsl - expressions - sts
forceZero
	"DIES"
	^ [ :s :c | 
	s = 0
		ifTrue: [ { s -> 1 when: {} } ]
		ifFalse: [ { "s -> (s+1) when: { die }" } ] ]