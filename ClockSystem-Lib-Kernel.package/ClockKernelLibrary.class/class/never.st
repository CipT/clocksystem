ccsl - expressions - sts
never
	"a clock that never ticks. we add the transition without clock so that the exploration mechanism ensures that the alphabet of the clock blocks its ticking"
	^[:s :c |
		{ s->s when: { }}
		]