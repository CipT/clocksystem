ccsl - relations - sts
precedence
	| warningBlock |
	warningBlock := [ :configuration |
		configuration first abs \\ 10 = 9
			ifTrue: [ 
				(self
					confirm:
						'one precedence relation exceeded ' , configuration first abs printString
							, ' states. Do you want to continue?')
			ifFalse: [ ^ self error ] ] ].
	^ [ :s :bound :a :b | 
	bound < 0
		ifTrue: [ 
			"unbounded precedence"
			s = 0
				ifTrue: [ 
					{(s -> (s + 1) when: {a} do: warningBlock).
					(s -> s
						when:
							{a.
							b})} ]
				ifFalse: [ 
					{(s -> s
						when:
							{a.
							b}).
					(s -> (s + 1) when: {a} do: warningBlock).
					(s -> (s - 1) when: {b} do: warningBlock)} ] ]
		ifFalse: [ 
			"bounded precedence"
			s = bound
				ifTrue: [ 
					s = 0
						ifTrue: [ 
							{(s -> s
								when:
									{a.
									b})} ]
						ifFalse: [ 
							{(s -> s
								when:
									{a.
									b}).
							(s -> (s - 1) when: {b})} ] ]
				ifFalse: [ 
					s = 0
						ifTrue: [ 
							{(s -> (s + 1) when: {a}).
							(s -> s
								when:
									{a.
									b})} ]
						ifFalse: [ 
							{(s -> s
								when:
									{a.
									b}).
							(s -> (s + 1) when: {a}).
							(s -> (s - 1) when: {b})} ] ] ] ]