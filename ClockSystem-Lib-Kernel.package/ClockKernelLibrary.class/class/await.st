ccsl - expressions - sts
await
	"the awaiting clock expression a^n ticks in coincidence with the next nth strictly future tick of a, and then dies"
	"DIES"
	^[:s :a :c |
		s = 1 ifTrue: [ 1->0 when: { a. c } ]
			ifFalse: [ s->(s-1) when: { a } ]
	]
	