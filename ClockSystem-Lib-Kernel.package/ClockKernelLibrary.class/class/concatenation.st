ccsl - expressions - sts
concatenation
 	^[:s :a :b :c |
		a isDead ifFalse: [{ s->s when: { a. c }.
				s->s when: { b }.
				s->s when: { a. b. c } }]
			ifTrue: [
				{ s->1 when: { b. c } } 
			]
		]