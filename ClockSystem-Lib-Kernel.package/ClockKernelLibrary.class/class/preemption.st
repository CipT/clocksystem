ccsl - expressions - sts
preemption
	"the preemption expression behaves as a while b does not tick. When b ticks, the expression dies"
	"DIES"
	^[:s :a :b :c |
		s = 0 
			ifTrue: [ { s -> s when: { a. c }.
				s -> (s+1) when: { b }. 	"DIES"
				s -> (s+1) when: { a. b }. 	"DIES" } ]
			ifFalse: [ { s -> s when: { a. b }.
				s -> s when: { a }.
				s -> s when: { b } } ]
		
	]
	