ccsl - expressions - sts
delay
	"after n ticks of the base clock, the base clock and the delayed one become synchronous forever "
	^[:s :n :a :c |
		n < 1 ifTrue: [ {s->s when: { a. c }} ]
			ifFalse: [ {s->(s+1) when: { a } do: [ :configuration | |m| 
													m := configuration second.  
													configuration at: 2 put: m - 1 ] }]	
	]