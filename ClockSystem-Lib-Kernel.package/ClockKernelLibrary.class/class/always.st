ccsl - expressions - sts
always
	"The always automaton need particular treatment during exploration. 
	since the transition should be always forced"
	"The clock always tick"
	^[:s :c |
		{ s->s when: { c }}
		]