ccsl - expressions - sts
deferred
	^[ :s :ns :a :b :c |
	s = 0 ifTrue: [ { s->s when: { b }.
			s->(s+2) when: { a }.
			s->(s+2) when: { a. b } } ]
		ifFalse: [ 
			| sync advance next|
			sync := nil.
			((1 to: (s-1)) anySatisfy: [ :i | (ns at: i) = 1 ]) ifTrue: [ 
				sync := { b. c }]
			ifFalse: [ sync := { b } ].
			advance := [:configuration | |state list|
				state := configuration first.
				list := configuration second.
				1 to: (state-1) do: [:i |  |lI|
					lI := list at: i.
					lI > 0 ifTrue: [ list at: i put: (lI-1) ] ] ].
			next := (ns size < s ifTrue: [s] ifFalse: [s+1]).
			{
			s->s when: sync do: advance.
			s->next when: { a }.
			s->next when: { a }, sync do: advance} ] ].