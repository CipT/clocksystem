ccsl - expressions - sts
intersection
 	^[:s :a :b :c |
		{ s->s when: { a }.
		s->s when: { b }.
		s->s when: { a. b. c } }
		]